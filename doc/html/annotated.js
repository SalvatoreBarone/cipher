var annotated =
[
    [ "AES128_key_t", "struct_a_e_s128__key__t.html", "struct_a_e_s128__key__t" ],
    [ "AES128_roundKeys_t", "struct_a_e_s128__round_keys__t.html", "struct_a_e_s128__round_keys__t" ],
    [ "AES_dataBlock_t", "struct_a_e_s__data_block__t.html", "struct_a_e_s__data_block__t" ],
    [ "DES_dataBlock_t", "struct_d_e_s__data_block__t.html", "struct_d_e_s__data_block__t" ],
    [ "DES_expanded48_t", "struct_d_e_s__expanded48__t.html", "struct_d_e_s__expanded48__t" ],
    [ "DES_key_t", "struct_d_e_s__key__t.html", "struct_d_e_s__key__t" ],
    [ "DES_reducedKey_t", "struct_d_e_s__reduced_key__t.html", "struct_d_e_s__reduced_key__t" ],
    [ "DES_roundKey_t", "struct_d_e_s__round_key__t.html", "struct_d_e_s__round_key__t" ],
    [ "DES_roundKeys_t", "struct_d_e_s__round_keys__t.html", "struct_d_e_s__round_keys__t" ],
    [ "DES_splitted28_t", "struct_d_e_s__splitted28__t.html", "struct_d_e_s__splitted28__t" ],
    [ "DES_splitted32_t", "struct_d_e_s__splitted32__t.html", "struct_d_e_s__splitted32__t" ],
    [ "RM_1_7_codeword_t", "struct_r_m__1__7__codeword__t.html", "struct_r_m__1__7__codeword__t" ],
    [ "SHA256_sum_t", "struct_s_h_a256__sum__t.html", "struct_s_h_a256__sum__t" ],
    [ "SHA512_sum_t", "struct_s_h_a512__sum__t.html", "struct_s_h_a512__sum__t" ]
];