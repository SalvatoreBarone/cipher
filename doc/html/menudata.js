var menudata={children:[
{text:"Pagina Principale",url:"index.html"},
{text:"Pagine collegate",url:"pages.html"},
{text:"Moduli",url:"modules.html"},
{text:"Strutture dati",url:"annotated.html",children:[
{text:"Strutture dati",url:"annotated.html"},
{text:"Indice delle strutture dati",url:"classes.html"},
{text:"Campi dei dati",url:"functions.html",children:[
{text:"Tutto",url:"functions.html"},
{text:"Variabili",url:"functions_vars.html"}]}]},
{text:"File",url:"files.html",children:[
{text:"Elenco dei file",url:"files.html"},
{text:"Elementi globali",url:"globals.html",children:[
{text:"Tutto",url:"globals.html",children:[
{text:"_",url:"globals.html#index__"},
{text:"a",url:"globals.html#index_a"},
{text:"b",url:"globals.html#index_b"},
{text:"c",url:"globals.html#index_c"},
{text:"d",url:"globals.html#index_d"},
{text:"f",url:"globals.html#index_f"},
{text:"g",url:"globals.html#index_g"},
{text:"i",url:"globals.html#index_i"},
{text:"m",url:"globals.html#index_m"},
{text:"p",url:"globals.html#index_p"},
{text:"q",url:"globals.html#index_q"},
{text:"r",url:"globals.html#index_r"},
{text:"s",url:"globals.html#index_s"},
{text:"t",url:"globals.html#index_t"}]},
{text:"Funzioni",url:"globals_func.html",children:[
{text:"a",url:"globals_func.html#index_a"},
{text:"c",url:"globals_func.html#index_c"},
{text:"d",url:"globals_func.html#index_d"},
{text:"m",url:"globals_func.html#index_m"},
{text:"r",url:"globals_func.html#index_r"},
{text:"s",url:"globals_func.html#index_s"},
{text:"t",url:"globals_func.html#index_t"}]},
{text:"Variabili",url:"globals_vars.html"},
{text:"Tipi enumerati (enum)",url:"globals_enum.html"},
{text:"Valori del tipo enumerato",url:"globals_eval.html"},
{text:"Definizioni",url:"globals_defs.html"}]}]},
{text:"Esempi",url:"examples.html"}]}
