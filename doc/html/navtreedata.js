var NAVTREE =
[
  [ "Cipher", "index.html", [
    [ "Lista dei test", "test.html", null ],
    [ "Moduli", "modules.html", "modules" ],
    [ "Strutture dati", "annotated.html", [
      [ "Strutture dati", "annotated.html", "annotated_dup" ],
      [ "Indice delle strutture dati", "classes.html", null ],
      [ "Campi dei dati", "functions.html", [
        [ "Tutto", "functions.html", null ],
        [ "Variabili", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "File", null, [
      [ "Elenco dei file", "files.html", "files" ],
      [ "Elementi globali", "globals.html", [
        [ "Tutto", "globals.html", null ],
        [ "Funzioni", "globals_func.html", null ],
        [ "Variabili", "globals_vars.html", null ],
        [ "Tipi enumerati (enum)", "globals_enum.html", null ],
        [ "Valori del tipo enumerato", "globals_eval.html", null ],
        [ "Definizioni", "globals_defs.html", null ]
      ] ]
    ] ],
    [ "Esempi", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"_reed_muller_8c.html"
];

var SYNCONMSG = 'cliccare per disabilitare la sincronizzazione del pannello';
var SYNCOFFMSG = 'cliccare per abilitare la sincronizzazione del pannello';