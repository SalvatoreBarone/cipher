var indexSectionsWithContent =
{
  0: "_abcdefghilmpqrst",
  1: "adrs",
  2: "adrs",
  3: "acdmrst",
  4: "bgimpqrs",
  5: "t",
  6: "fi",
  7: "_bcimrst",
  8: "adehrs",
  9: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "Tutto",
  1: "Strutture dati",
  2: "File",
  3: "Funzioni",
  4: "Variabili",
  5: "Tipi enumerati (enum)",
  6: "Valori del tipo enumerato",
  7: "Definizioni",
  8: "Gruppi",
  9: "Pagine"
};

