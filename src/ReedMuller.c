/**
 * @file ReedMuller.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "ReedMuller.h"

// test del bit n-esimo di row, riga di una matrice.
// Il bit numero 0 è il bit 0 della locazione ad indirizzo 15, il bit 128 è il bit 7 della locazione ad indirizzo 0.
// la numerazione segue la significativita' dei bit all'interno dei singoli byte
// restituisce 1 se il bit è 1, 0 altrimenti
#define testBit(row, n) (((row[15-(n)/8]&(((uint8_t)1)<<((n)%8)))==0) ? 0 : 1)

// reset del bit n-esimo di var.
#define clearBit(var, n) var&= ~(((uint8_t)1)<<(n))

// set del bit n-esimo di var.
#define setBit(var, n) var|=(((uint8_t)1)<<(n))

// matrice di generazione
const uint8_t genMatrix[8][16] = {
	{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
	{0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff},
	{0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0xff, 0xff},
	{0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff},
	{0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f},
	{0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33},
	{0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55}
};

void RM_1_7_encode(uint8_t byte, RM_1_7_codeword_t *codeword)
{
	int i, j;
	for (i=0; i < 16; i++)
		codeword->byte_array[i]=((byte&0x80)==0 ? 0 : genMatrix[0][i]);
	for (i=6; i!=-1; i--)
		if ((byte&(1<<i))!=0)
			for (j=0; j<16; j++)
				codeword->byte_array[j]^=genMatrix[7-i][j];
}

uint8_t RM_1_7_decode(RM_1_7_codeword_t *codeword)
{
	uint8_t byte = 0;
	int i, j, k, next = 1, period = 2, one_count, m = 7;
	// calcolo dei primi m-bit meno significativi, a partire dal bit 0
	for (i=0; i<m; i++, next<<=1, period<<=1) {
		one_count = 0;
		for (j=0; j<(1<<m); j+=period)
			for(k=0; k<next; k++)
				one_count += (testBit(codeword->byte_array, j+k) == testBit(codeword->byte_array, j+k+next) ? 0 : 1);
		if (one_count >= (1<<(m-2)))
			setBit(byte, i);
//		else
//			clearBit(byte, i);
	}
	// calcolo del bit m+1-esimo, quello piu' significativo
	// copio la codeword, per non agire direttamente su di essa
	uint8_t tmp[16];
	for (i=0; i<16; i++)
		tmp[i] = codeword->byte_array[i];
	// costrisco la "matrice", in realta' le XOR vengono effettuate su tmp, senza la necessita' di allocare una matrice
	for (i=6; i!=-1; i--)
		if ((byte&(1<<i))!=0)
			for (j=0; j<16; j++)
				tmp[j]^=genMatrix[7-i][j];
	// dopo aver calcolato la XOR, conto gli uno
	one_count = 0;
	for (i=0; i<(1<<7); i++)
		one_count += testBit(tmp, i);
	if (one_count >= (1<<(m-2)))
		setBit(byte, 7);
//	else
//		clearBit(byte, 7);

	return byte;
}
