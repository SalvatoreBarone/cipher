/**
 * @file ReedMuller.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

/**
 * @addtogroup ECC
 * @{
 *
 * @brief Implementazione in C di alcuni Error Correction Code
 *
 * @defgroup Reed-Muller
 * @{
 *
 * @brief Implementazione in C degli error-correction-code di Reed-Muller.
 *
 * L'implementazione fa riferimento ai codici di Reed-Muller(1,7) - cioe' con r=1 ed m=8 - i quali permettono di codificare una
 * word lunga 8 bit in una codeword lunga 128 bit, permettendo di correggere fino a 2^(m-2)-1=31 errori all'interno di essa.
 * La codifica viene effettuata con una matrice di generazione in forma canonica con zeri in testa. Nello specifico, la matrice
 * usata viene riportata di seguito, in notazione esadecimale.<br>
 * ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff<br>
 * 00 00 00 00 00 00 00 00 ff ff ff ff ff ff ff ff<br>
 * 00 00 00 00 ff ff ff ff 00 00 00 00 ff ff ff ff<br>
 * 00 00 ff ff 00 00 ff ff 00 00 ff ff 00 00 ff ff<br>
 * 00 ff 00 ff 00 ff 00 ff 00 ff 00 ff 00 ff 00 ff<br>
 * 0f 0f 0f 0f 0f 0f 0f 0f 0f 0f 0f 0f 0f 0f 0f 0f<br>
 * 33 33 33 33 33 33 33 33 33 33 33 33 33 33 33 33<br>
 * 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55<br>
 */

#ifndef __REED_MULLER_ECC_H__
#define __REED_MULLER_ECC_H__

#include <inttypes.h>

/**
 * @brief Struttura che rappresenta una codeword codificata con un codice di Reed-Muller(1,7)
 */
typedef struct {
	uint8_t byte_array[16]; /**< 	array di byte contenenti la codeword. Il byte di indice 0 e' il byte piu' significativo,
									quello di indice 15 il meno significativo. */
} RM_1_7_codeword_t;

/**
 * @brief Permette di effettuare la codifica con error-correction-code di Reed-Muller RM(1,7), codificando 8 bit in 128 bit.
 *
 * @param[in]	byte		byte da codificare
 * @param[out]	codeword	puntatore a struttura RM_1_7_codeword, conterra' la codeword codificata con RM(1,7).
 *
 */
void RM_1_7_encode(uint8_t byte, RM_1_7_codeword_t *codeword);

/**
 * @brief Permette di effettuare la decodifica con error-correction-code di Reed-Muller RM(1,7).
 *
 * @param[in] codeword	puntatore a struttura RM_1_7_codeword, contenente la codeword codificata con RM(1,7) da decodificare
 * @return				byte decodificato corrispondente alla codeword
 *
 */
uint8_t RM_1_7_decode(RM_1_7_codeword_t *codeword);

#endif

/**
 * @example rm_test.c
 * Il file rm_test.c contiene un esempio d'uso delle funzioni RM_1_7_encode() e RM_1_7_decode(), le quali
 * effettuano, rispettivamente, codifica e decodifica di un byte con codice di Reed-Muller(1,7). Nella
 * codeword ottenuta dopo il passo di codifica viene inserito un "rumore casuale", introdotto invertendo
 * in modo casuale un certo numero di bit della codeword. La codeword in cui è stato inserito rumore, viene,
 * poi, decodificata. Il codice di correzione dell'errore assicura che, anche con la presenza di un errore
 * costituito da al più \f$2^{5}-1=31\f$ bit, è possibile riottenere il byte originario.
 */

/**
 * @}
 * @}
 */
