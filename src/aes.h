/**
 * @file aes.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

/**
 * @addtogroup SimmetricCipher
 * @{
 *
 * @brief Implementazione di alcuni algoritmi di crittografia a chiave simmetrica in C.
 *
 * @defgroup AES
 * @{
 *
 * @brief Implementazione C dell'algoritmo di cifratura AES.
 *
 */


#ifndef __AES_HEADER_H__
#define __AES_HEADER_H__

#include <inttypes.h>

/**
 * @brief Struttura che astrae un blocco dati (plaintext o ciphertext) che l'algoritmo AES deve processare.
 */
typedef struct {
	uint8_t byte_array[16]; /**< array di 16 byte (128 bit) contenente i dati che l'algoritmo AES deve processare */
} AES_dataBlock_t;

/**
 * @brief Struttura che astrae la chiave simmetrica di cifratura/decifratura
 */
typedef struct {
	uint8_t byte_array[16]; /**< array di 16 byte (128 bit) contenente la chiave */
} AES128_key_t;

/**
 * @brief Struttura che astrae le sottochiavi usate dall'algoritmo AES in ciascuno dei round di cifratura/decifratura
 */
typedef struct {
	uint8_t byte_array[11][16]; /**< Matrice di 11 righe e 16 colonne. Ciascuna delle righe e' un array di 16 byte contenente una sottochiave
	 	 	 	 	 	 	 	 	 per uno specifico dei round di cifratura/decifratura */
} AES128_roundKeys_t;


/**
 * @brief Generatore di round-key per AES-128
 *
 * Consente di generare la chiave espansa per la cifratura/decifratura con AES-128
 * @param[in]	key			puntatore a struttura AES128_key_t, contenente la chiave segreta di cifratura;
 * @param[out]	subkeys		puntatore a struttura AES128_roundKeys_t, conterra'	le sottochiavi usate da AES128 in ognuno dei round di cifratura;
 *
 * @code
 * AES128_key_t key = {.byte_array = {0x0F, 0x15, 0x71, 0xC9, 0x47, 0xD9, 0xE8, 0x59, 0x0C, 0xB7, 0xAD, 0xD6, 0xAF, 0x7F, 0x67, 0x98}};
 * AES128_roundKeys_t subkeys;
 * AES128_subkeyGenerator(&key, &subkeys);
 * @endcode
 *
 * @test
 * key:      0F 15 71 C9 47 D9 E8 59 0C B7 AD D6 AF 7F 67 98<br>
 * Subkeys:<br>
 * round 0:  0F 15 71 C9 47 D9 E8 59 0C B7 AD D6 AF 7F 67 98<br>
 * round 1:  DC 90 37 B0 9B 49 DF E9 97 FE 72 3F 38 81 15 A7<br>
 * round 2:  D2 C9 6B B7 49 80 B4 5E DE 7E C6 61 E6 FF D3 C6<br>
 * round 3:  C0 AF DF 39 89 2F 6B 67 57 51 AD 06 B1 AE 7E C0<br>
 * round 4:  2C 5C 65 F1 A5 73 0E 96 F2 22 A3 90 43 8C DD 50<br>
 * round 5:  58 9D 36 EB FD EE 38 7D 0F CC 9B ED 4C 40 46 BD<br>
 * round 6:  71 C7 4C C2 8C 29 74 BF 83 E5 EF 52 CF A5 A9 EF<br>
 * round 7:  37 14 93 48 BB 3D E7 F7 38 D8 08 A5 F7 7D A1 4A<br>
 * round 8:  48 26 45 20 F3 1B A2 D7 CB C3 AA 72 3C BE 0B 38<br>
 * round 9:  FD 0D 42 CB 0E 16 E0 1C C5 D5 4A 6E F9 6B 41 56<br>
 * round 10: B4 8E F3 52 BA 98 13 4E 7F 4D 59 20 86 26 18 76<br>
 */
void AES128_subkeyGenerator(AES128_key_t *key, AES128_roundKeys_t *subkeys);

/**
 * @brief Cifratura con AES-128
 *
 * Consente di cifrare un blocco di 16 uint8_t (128 bit) usando AES-128
 *
 * @param[in] 	plaintext	puntatore a struttura AES_dataBlock_t contenente il plaintext da cifrare
 * @param[in] 	subkeys		puntatore a struttura AES128_roundKeys_t contenente	le sottochiavi usate da AES128 in ognuno dei round di cifratura
 * @param[out]	ciphertext	puntatore a struttura AES_dataBlock_t, conterra' il ciphertext
 *
 * @code
 * AES128_key_t key = {.byte_array = {0x0F, 0x15, 0x71, 0xC9, 0x47, 0xD9, 0xE8, 0x59, 0x0C, 0xB7, 0xAD, 0xD6, 0xAF, 0x7F, 0x67, 0x98}};
 * AES128_roundKeys_t subkeys;
 * AES_dataBlock_t plaintext = {.byte_array = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0xFE, 0xDC, 0xBA, 0x98, 0x76, 0x54, 0x32, 0x10}};
 * AES_dataBlock_t ciphertext;
 * AES128_subkeyGenerator(&key, &subkeys);
 * AES128_encrypt(&plaintext, &subkeys, &ciphertext);
 * // stampa del ciphertext in esadecimale
 * printf("\nciphertext:\t");
 * for (i = 0; i < 16; i++)
 * 	printf("%02X ", ciphertext.byte_array[i]);
 * printf("\n");
 * @endcode
 *
 * @test
 * plaintext:	01 23 45 67 89 AB CD EF FE DC BA 98 76 54 32 10<br>
 * key:         0F 15 71 C9 47 D9 E8 59 0C B7 AD D6 AF 7F 67 98<br>
 * ciphertext:	FF 0B 84 4A 08 53 BF 7C 69 34 AB 43 64 14 8F B9<br>
 */
void AES128_encrypt(AES_dataBlock_t *plaintext, AES128_roundKeys_t *subkeys, AES_dataBlock_t *ciphertext);

/**
 * @brief Decifratura con AES-128
 *
 * Consente di decifrare un blocco di 16 uint8_t (128 bit) precedentemente cifrato usando AES-128
 *
 * @param[in]	ciphertext	puntatore a struttura AES_dataBlock_t contenente il ciphertext da decifrare
 * @param[in]	subkeys		puntatore a struttura AES128_roundKeys_t contenente	le sottochiavi usate da AES128 in ognuno dei round di cifratura
 * @param[out]	plaintext	puntatore a struttura AES_dataBlock_t, conterra' il plaintext decifrato
 *
 * @code
 * AES128_key_t key = {.byte_array = {0x0F, 0x15, 0x71, 0xC9, 0x47, 0xD9, 0xE8, 0x59, 0x0C, 0xB7, 0xAD, 0xD6, 0xAF, 0x7F, 0x67, 0x98}};
 * AES128_roundKeys_t subkeys;
 * AES_dataBlock_t ciphertext = {.byte_array = {0xFF, 0x0B, 0x84, 0x4A, 0x08, 0x53, 0xBF, 0x7C, 0x69, 0x34, 0xAB, 0x43, 0x64, 0x14, 0x8F, 0xB9}};
 * AES_dataBlock_t plaintext;
 * AES128_subkeyGenerator(&key, &subkeys);
 * AES128_decrypt(&ciphertext, &subkeys, &plaintext);
 * printf("\nplaintext:\t");
 * for (i = 0; i < 16; i++)
 * 	printf("%02X ", deciphered.byte_array[i]);
 * printf("\n");
 * @endcode
 *
 * @test
 * ciphertext:	FF 0B 84 4A 08 53 BF 7C 69 34 AB 43 64 14 8F B9<br>
 * key:         0F 15 71 C9 47 D9 E8 59 0C B7 AD D6 AF 7F 67 98<br>
 * plaintext:	01 23 45 67 89 AB CD EF FE DC BA 98 76 54 32 10<br>
 */
void AES128_decrypt(AES_dataBlock_t *ciphertext, AES128_roundKeys_t *subkeys, AES_dataBlock_t *plaintext);

/**
 * @example aes_test.c
 * Il file aes_test.c contiene un esempio d'uso delle funzioni AES128_subkeyGenerator(), AES128_encrypt() e
 * AES128_decrypt() per cifrare e decifrare blocchi di byte.
 */

/**
 * @}
 * @}
 */

#endif
