/**
 * @file des.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include "des.h"

#ifdef __DES_TRACE__
#include <stdio.h>
#endif


/**
 * @brief Struttura che astrae la chiave simmetrica di cifratura/decifratura PERMUTATA e RIDOTTA da PC1
 */
typedef struct {
	uint8_t byte_array[7]; /**< array di 7 byte contenente la chiave simmetrica di cifratura/decifratura dopo la permutazione PC1 */
} DES_reducedKey_t;

/**
 * @brief Struttura che astrae uno spezzone da 28 bit in cui viene divisa la chiave dopo la permutazione PC1
 */
typedef struct {
	uint8_t byte_array[4]; /**< array di 4 byte, 32 bit in totale. Solo i 28 bit meno significativi saranno usati */
} DES_splitted28_t;

/**
 * @brief Struttura che astrae uno spezzone da 32 bit in cui viene diviso il blocco di dati da cifrare, dopo aver effettuato la permutazione IP
 */
typedef struct {
	uint8_t byte_array[4]; /**< array di 4 byte, 32 bit in totale */
} DES_splitted32_t;

/**
 * @brief Struttura che astrae uno spezzone da 48 bit ottenuto mediante espansione di un blocco di 32 bit
 */
typedef struct {
	uint8_t byte_array[6]; /**< array di 6 byte, 48 bit in totale */
} DES_expanded48_t;


/**
 * @brief Inizializza a zero una struttura DES_reducedKey_t
 */
#define INIT_DES_reducedKey_t {.byte_array = { 0, 0, 0, 0, 0, 0, 0}}

/**
 * @brief Inizializza a zero una struttura DES_splitted28_t
 */
#define INIT_DES_splitted28_t {.byte_array = { 0, 0, 0, 0}}

/**
 * @brief Inizializza a zero una struttura DES_splitted32_t
 */
#define INIT_DES_splitted32_t {.byte_array = { 0, 0, 0, 0}}


/**
 * @brief Inizializza a zero una struttura DES_roundKey_t
 */
#define INIT_DES_roundKey_t {.byte_array = {0, 0, 0, 0, 0, 0}}

/**
 * Test del bit n-esimo di una word costituita da un array di byte.
 * La numerazione dei bit segue la significativita' dei bit all'interno dei singoli byte, cioe' il bit numero 0 e' il bit 0 della locazione
 * ad indirizzo nword, il bit 8 è il bit 0 della locazione ad indirizzo 1, e cose' via.
 *
 * @param[in] word	array di byte (uint8_t)
 * @param[in] nbyte indice dell'ultima locazione dell'array word
 * @param[in] nbit	bit da testare
 *
 * @retval 1, se il bit e' 1;
 * @retval 0, se il bit e' 0;
 */
static inline uint8_t TestBit(uint8_t *word, uint8_t nbyte, uint8_t nbit) {
	return (((word[nbyte-(nbit)/8]&(1<<((nbit)%8)))==0) ? 0 : 1);
}

/**
 * Set del bit n-esimo di una word costituita da un array di byte.
 * La numerazione dei bit segue la significativita' dei bit all'interno dei singoli byte, cioe' il bit numero 0 e' il bit 0 della locazione
 * ad indirizzo nword, il bit 8 è il bit 0 della locazione ad indirizzo 1, e cose' via.
 *
 * @param[inout]	word	array di byte (uint8_t)
 * @param[in]		nbyte	indice dell'ultima locazione dell'array word
 * @param[in]		nbit	bit da settare
 */
static inline void SetBit(uint8_t *word, uint8_t nbyte, uint8_t nbit) {
	word[nbyte-(nbit)/8] |= (1<<((nbit)%8));
}

/**
 * Clear del bit n-esimo di una word costituita da un array di byte.
 * La numerazione dei bit segue la significativita' dei bit all'interno dei singoli byte, cioe' il bit numero 0 e' il bit 0 della locazione
 * ad indirizzo nword, il bit 8 è il bit 0 della locazione ad indirizzo 1, e cose' via.
 *
 * @param[inout]	word	array di byte (uint8_t)
 * @param[in]		nbyte	indice dell'ultima locazione dell'array word
 * @param[in]		nbit	bit da settare
 */
static inline void ClearBit(uint8_t *word, uint8_t nbyte, uint8_t nbit) {
	word[nbyte-(nbit)/8] &= ~(1<<((nbit)%8));
}

/**
 * @brief DES_PC1Permutation effettua la permutazione PC1 sulla chiave di cifratura, ottenendo una chiave permutata di 56 bit
 *
 * @param[in] 	key			puntatore a DES_key_t, contenente la chiave di cifratura/decifratura
 * @param[out]	permutedKey	puntatore a DES_permutedKey_t, contenente la chiave permutata
 */
void DES_PC1Permutation(DES_key_t *key, DES_reducedKey_t *permutedKey) {
	// Permutazione PC1
	static const uint8_t PC1[] = {
		7,	15,	23,	31,	39,	47,	55,	63,
		6,	14,	22,	30,	38,	46,	54,	62,
		5,	13,	21,	29,	37,	45,	53,	61,
		4,	12,	20,	28,	1,	9,	17,	25,
		33,	41,	49,	57,	2,	10,	18,	26,
		34,	42,	50,	58,	3,	11,	19,	27,
		35,	43,	51,	59,	36,	44,	52,	60
	};

	uint8_t i = 0;
	for (i=0; i<56; i++) {
		if (TestBit(key->byte_array, 7, PC1[i]))
			// anche se permutedKey->byte_array e' di 7 bit anziche' 8, viene considerato essere
			// 8 byte per semplificare la gestione degli indici
			SetBit(permutedKey->byte_array, 6, 56-i-1);
		else
			ClearBit(permutedKey->byte_array, 6, 56-i-1);
	}
}

/**
 * @brief Divide la chiave a 56 bit ottenuta dopo la permutazione PC1, in due blocchi, ciascuno dei quali da 28 bit
 *
 * @param[in] reducedKey	puntatore a struttura DES_reducedKey_t, contenente la chiave a 56 bit ottenuta dopo la permutazione PC1
 * @param[in] C				puntatore a struttura DES_splitted28_t, conterra' i 28 bit piu' significativi di reducedKey
 * @param[in] D				puntatore a struttura DES_splitted28_t, conterra' i 28 bit meno significativi di reducedKey
 */
void DES_split28(DES_reducedKey_t *reducedKey, DES_splitted28_t *C, DES_splitted28_t *D) {
	D->byte_array[3] = reducedKey->byte_array[6];
	D->byte_array[2] = reducedKey->byte_array[5];
	D->byte_array[1] = reducedKey->byte_array[4];
	D->byte_array[0] = reducedKey->byte_array[3]&0xF;
	C->byte_array[3] = (reducedKey->byte_array[3]>>4) | ((reducedKey->byte_array[2]&0xF)<<4);
	C->byte_array[2] = (reducedKey->byte_array[2]>>4) | ((reducedKey->byte_array[1]&0xF)<<4);
	C->byte_array[1] = (reducedKey->byte_array[1]>>4) | ((reducedKey->byte_array[0]&0xF)<<4);
	C->byte_array[0] = (reducedKey->byte_array[0]>>4);
}

/**
 * @brief Effettua lo shift circolare a sinistra di shamt bit, del contenuto di una struttura DES_splitted28_t
 *
 * @param[inout]	C		puntatore a struttura DES_splitted28_t, contenente i bit da shiftare
 * @param[in]		shamt	numero di posizioni da shiftare
 */
void DES_shift28(DES_splitted28_t *C, uint8_t shamt) {
	uint8_t tmp = C->byte_array[0]>>(4-shamt);
	C->byte_array[0] = ((C->byte_array[0]<<shamt)&0xF) | C->byte_array[1]>>(8-shamt);
	C->byte_array[1] = (C->byte_array[1]<<shamt) | (C->byte_array[2]>>(8-shamt));
	C->byte_array[2] = (C->byte_array[2]<<shamt) | (C->byte_array[3]>>(8-shamt));
	C->byte_array[3] = (C->byte_array[3]<<shamt) | tmp;
}

/**
 * @brief Effettua il merging di due stringhe di 28 bit ciascuna, per ottenere una stringa da 56 bit
 *
 * @param[in]	C			puntatore a struttura DES_splitted28_t, contenente i 28 bit piu' significativi
 * @param[in]	D			puntatore a struttura DES_splitted28_t, contenente i 28 bit meno significativi
 * @param[out]	mergedKey	puntatore a struttura DES_reducedKey_t, conterra' i 56 bit ottenuti effettuando il merging di C e D
 */
void DES_merge28(DES_splitted28_t *C, DES_splitted28_t *D, DES_reducedKey_t *mergedKey) {
	mergedKey->byte_array[0] = (C->byte_array[0]<<4) | (C->byte_array[1]>>4);
	mergedKey->byte_array[1] = (C->byte_array[1]<<4) | (C->byte_array[2]>>4);
	mergedKey->byte_array[2] = (C->byte_array[2]<<4) | (C->byte_array[3]>>4);
	mergedKey->byte_array[3] = (C->byte_array[3]<<4) | D->byte_array[0];
	mergedKey->byte_array[4] = D->byte_array[1];
	mergedKey->byte_array[5] = D->byte_array[2];
	mergedKey->byte_array[6] = D->byte_array[3];
}

/**
 * @brief DES_PC2Permutation effettua la permutazione PC2
 *
 * Effettua la permutazione PC2 su una struttura DES_reducedKey_t, contenente 56 bit,per produrre una struttura DES_roundKey_t, contenente
 * 48 bit, corrispondente ad una delle sottochiavi usate in uno dei round dell'algoritmo DES
 *
 * @param[in]	reducedKey	puntatore a struttura DES_reducedKey_t, contenente i 56 bit da permutare
 * @param[out]	subKey		puntatore a struttura DES_roundKey_t, conterra' i 48 bit prodotti dalla permutazione
 */
void DES_PC2Permutation(DES_reducedKey_t *reducedKey, DES_roundKey_t *subKey) {
	int i;
	// Permutazione PC2
	static const uint8_t PC2[] = {
		42,	39,	45,	32,	55,	51,	53,	28,
		41,	50,	35,	46,	33,	37,	44,	52,
		30,	48,	40,	49,	29,	36,	43,	54,
		15,	4,	25,	19,	9,	1,	26,	16,
		5,	11,	23,	8,	12,	7,	17,	0,
		22,	3,	10,	14,	6,	20,	27,	24
	};

	for (i=0; i<48; i++) {
		if (TestBit(reducedKey->byte_array, 6, PC2[i]))
			// anche se permutedKey->byte_array e' di 6 bit anziche' 8, viene considerato essere
			// 8 byte per semplificare la gestione degli indici
			SetBit(subKey->byte_array, 5, 48-i-1);
		else
			ClearBit(subKey->byte_array, 5, 48-i-1);
	}
}

void DES_subkeyGenerator(DES_key_t *key, DES_roundKeys_t *subkeys) {
	// Shift-amount delle word delle chiavi
	static const uint8_t shamt[] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};
	int i;
	DES_reducedKey_t reducedKey = INIT_DES_reducedKey_t;
	DES_PC1Permutation(key, &reducedKey);
	DES_splitted28_t C = INIT_DES_splitted28_t, D = INIT_DES_splitted28_t;
	DES_split28(&reducedKey, &C, &D);

	for (i=0; i<16; i++) {
		DES_shift28(&C, shamt[i]);
		DES_shift28(&D, shamt[i]);
		DES_merge28(&C, &D, &reducedKey);
		DES_PC2Permutation(&reducedKey, &(subkeys->subkey[i]));
	}
}

/**
 * @brief Effettua la permutazione IP su un blocco di dati
 *
 * @param[in]	data_in		puntatore a struttura DES_dataBlock_t, contenente il blocco di 64 bit da permutare
 * @param[out]	data_out	puntatore a struttura DES_dataBlock_t, conterra' il blocco da 64 bit permutato
 */
void DES_IPPermutation(DES_dataBlock_t *data_in, DES_dataBlock_t *data_out) {
	// Permutazione IP
	static const uint8_t IP[] = {
		6,	14,	22,	30,	38,	46,	54,	62,
		4,	12,	20,	28,	36,	44,	52,	60,
		2,	10,	18,	26,	34,	42,	50,	58,
		0,	8,	16,	24,	32,	40,	48,	56,
		7,	15,	23,	31,	39,	47,	55,	63,
		5,	13,	21,	29,	37,	45,	53,	61,
		3,	11,	19,	27,	35,	43,	51,	59,
		1,	9,	17,	25,	33,	41,	49,	57
	};

	int i;
	for (i=0; i<64; i++) {
		if (TestBit(data_in->byte_array, 7, IP[i]))
			// anche se permutedKey->byte_array e' di 6 bit anziche' 8, viene considerato essere
			// 8 byte per semplificare la gestione degli indici
			SetBit(data_out->byte_array, 7, 64-i-1);
		else
			ClearBit(data_out->byte_array, 7, 64-i-1);
	}
}

/**
 * @brief Divide un blocco di dati da 64 bit in due word da 32 bit
 * @param[in]	data_in blocco di dati da 64 bit
 * @param[out]	L 		32 bit piu' significativi di data_in, ossia i byte da 0 a 3
 * @param[out]	R 		32 bit meno significativi di data_in, ossia i byte da 4 a 7
 */
void DES_split32(DES_dataBlock_t *data_in, DES_splitted32_t *L, DES_splitted32_t *R) {
	L->byte_array[0] = data_in->byte_array[0];
	L->byte_array[1] = data_in->byte_array[1];
	L->byte_array[2] = data_in->byte_array[2];
	L->byte_array[3] = data_in->byte_array[3];
	R->byte_array[0] = data_in->byte_array[4];
	R->byte_array[1] = data_in->byte_array[5];
	R->byte_array[2] = data_in->byte_array[6];
	R->byte_array[3] = data_in->byte_array[7];
}

/**
 * @brief Effettua il merging di due stringhe di 32 bit ciascuna, per ottenere una stringa da 64 bit
 *
 * @param[in]	L			32 bit piu' significativi di data_out, ossia i byte da 0 a 3
 * @param[in]	R			32 bit meno significativi di data_out, ossia i byte da 4 a 7
 * @param[out]	data_out	blocco da 64bit costituito dall'unione dei blocchi L ed R
 */
void DES_merge32(DES_splitted32_t *L, DES_splitted32_t *R, DES_dataBlock_t *data_out) {
	data_out->byte_array[0] = L->byte_array[0];
	data_out->byte_array[1] = L->byte_array[1];
	data_out->byte_array[2] = L->byte_array[2];
	data_out->byte_array[3] = L->byte_array[3];
	data_out->byte_array[4] = R->byte_array[0];
	data_out->byte_array[5] = R->byte_array[1];
	data_out->byte_array[6] = R->byte_array[2];
	data_out->byte_array[7] = R->byte_array[3];
}

/**
 * @brief Copia byte a byte da una struttura DES_splitted32_t sorgente ad una DES_splitted32_t destinazione
 * @param[in] 	src	sorgente
 * @param[out]	dst destinazione
 */
#define COPY_DES_splitted32_t(src, dst) dst.byte_array[0] = src.byte_array[0];\
										dst.byte_array[1] = src.byte_array[1];\
										dst.byte_array[2] = src.byte_array[2];\
										dst.byte_array[3] = src.byte_array[3];

/**
 * @brief Effettua l'espansione di un blocco da 32bit in un blocco da 48 bit
 * @param[in]	dataBlock			puntatore a struttura DES_splitted32_t, contenente il blocco da 32 bit da espandere
 * @param[out]	expandedDataBlock	puntatore a struttura DES_expanded48_t, conterra' il blocco da 48 bit espanso
 */
void DES_expand(DES_splitted32_t *dataBlock, DES_expanded48_t *expandedDataBlock) {
	// Permutazione E
	static const uint8_t E[] = {
		31,	0,	1,	2,	3,	4,
		3,	4,	5,	6,	7,	8,
		7,	8,	9,	10, 11,	12,
		11,	12,	13,	14,	15,	16,
		15,	16,	17,	18,	19,	20,
		19,	20,	21,	22,	23,	24,
		23,	24,	25,	26,	27,	28,
		27,	28,	29,	30,	31,	0
	};

	int i;
	for (i=0; i<48; i++)
		if (TestBit(dataBlock->byte_array, 3, E[i]))
			SetBit(expandedDataBlock->byte_array, 5, i);
		else
			ClearBit(expandedDataBlock->byte_array, 5, i);
}

/**
 * @brief Effettua un'operazione di XOR bit a bit tra la sottochiave di round ed un blocco R espanso
 * @param[in]		roundKey			puntatore a struttura DES_roundKey_t, contenente la chiave
 * @param[inout]	expandedDataBlock	puntatore a struttura expandedDataBlock, contenente il blocco R espanso. Successivamente
 * 										conterra' il risultato dell'operazione di XOR
 */
void DES_XOR48(DES_roundKey_t *roundKey, DES_expanded48_t *expandedDataBlock) {
	int i;
	for (i=0; i<6; i++)
		expandedDataBlock->byte_array[i] ^= roundKey->byte_array[i];
}

/**
 * @brief Indicizza una sbox
 * @param[in]	sbox	indice della particolare S-box da indicizzare
 * @param[in]	index	indice della particolare locazione al quale si vuole accedere. Solo i 6 bit meno significativi verranno
 * 						presi in considerazione:
 * 						- bit 5 e bit 0 costituiranno l'indice di riga
 * 						- bit 4, 3, 2 ed 1 costiruiranno l'indice di colonna
 * @return Sbox[sbox][rowindex][colindex]
 */
#define SBOX_indexing(sbox, index)	S[sbox][((index&0x20)>>4)|(index&0x1)][(index>>1)&0xF]

/**
 * @brief Effettua la sostituzione/riduzione su un blocco da 48 bit, trasformandono il un blocco da 32 bit
 * @param[in]	expandedDataBlock	puntatore a DES_expanded48_t, contenente il blocco di 48 bit da sostituire/ridurre
 * @param[out]	dataBlock			puntatore a DES_splitted32_t, contenente il blocco di 32 bit prodotto dalla sostituzione/riduzione
 */
void DES_SReduce(DES_expanded48_t *expandedDataBlock, DES_splitted32_t *dataBlock) {
	// Sostituzione/riduzione S
	static const uint8_t S[8][4][16] = {
		{	{14,	4,	13,	1,	2,	15,	11,	8,	3,	10,	6,	12,	5,	9,	0,	7},
			{0,		15,	7,	4,	14,	2,	13,	1,	10,	6,	12,	11,	9,	5,	3,	8},
			{4,		1,	14,	8,	13,	6,	2,	11,	15,	12,	9,	7,	3,	10,	5,	0},
			{15,	12,	8,	2,	4,	9,	1,	7,	5,	11,	3,	14,	10,	0,	6,	13}},
		{	{15,	1,	8,	14,	6,	11,	3,	4,	9,	7,	2,	13,	12,	0,	5,	10},
			{3,		13,	4,	7,	15,	2,	8,	14,	12,	0,	1,	10,	6,	9,	11,	5},
			{0,		14,	7,	11,	10,	4,	13,	1,	5,	8,	12,	6,	9,	3,	2,	15},
			{13,	8,	10,	1,	3,	15,	4,	2,	11,	6,	7,	12,	0,	5,	14,	9}},
		{	{10,	0,	9,	14,	6,	3,	15,	5,	1,	13,	12,	7,	11,	4,	2,	8},
			{13,	7,	0,	9,	3,	4,	6,	10,	2,	8,	5,	14,	12,	11,	15,	1},
			{13,	6,	4,	9,	8,	15,	3,	0,	11,	1,	2,	12,	5,	10,	14,	7},
			{1,		10,	13,	0,	6,	9,	8,	7,	4,	15,	14,	3,	11,	5,	2,	12}},
		{	{7,		13,	14,	3,	0,	6,	9,	10,	1,	2,	8,	5,	11,	12,	4,	15},
			{13,	8,	11,	5,	6,	15,	0,	3,	4,	7,	2,	12,	1,	10,	14,	9},
			{10,	6,	9,	0,	12,	11,	7,	13,	15,	1,	3,	14,	5,	2,	8,	4},
			{3,		15,	0,	6,	10,	1,	13,	8,	9,	4,	5,	11,	12,	7,	2,	14}},
		{	{2,		12,	4,	1,	7,	10,	11,	6,	8,	5,	3,	15,	13,	0,	14,	9},
			{14,	11,	2,	12,	4,	7,	13,	1,	5,	0,	15,	10,	3,	9,	8,	6},
			{4,		2,	1,	11,	10,	13,	7,	8,	15,	9,	12,	5,	6,	3,	0,	14},
			{11,	8,	12,	7,	1,	14,	2,	13,	6,	15,	0,	9,	10,	4,	5,	3}},
		{	{12,	1,	10,	15,	9,	2,	6,	8,	0,	13,	3,	4,	14,	7,	5,	11},
			{10,	15,	4,	2,	7,	12,	9,	5,	6,	1,	13,	14,	0,	11,	3,	8},
			{9,		14,	15,	5,	2,	8,	12,	3,	7,	0,	4,	10,	1,	13,	11,	6},
			{4,		3,	2,	12,	9,	5,	15,	10,	11,	14,	1,	7,	6,	0,	8,	13}},
		{	{4,		11,	2,	14,	15,	0,	8,	13,	3,	12,	9,	7,	5,	10,	6,	1},
			{13,	0,	11,	7,	4,	9,	1,	10,	14,	3,	5,	12,	2,	15,	8,	6},
			{1,		4,	11,	13,	12,	3,	7,	14,	10,	15,	6,	8,	0,	5,	9,	2},
			{6,		11,	13,	8,	1,	4,	10,	7,	9,	5,	0,	15,	14,	2,	3,	12}},
		{	{13,	2,	8,	4,	6,	15,	11,	1,	10,	9,	3,	14,	5,	0,	12,	7},
			{1,		15,	13,	8,	10,	3,	7,	4,	12,	5,	6,	11,	0,	14,	9,	2},
			{7,		11,	4,	1,	9,	12,	14,	2,	0,	6,	10,	13,	15,	3,	5,	8},
			{2,		1,	14,	7,	4,	10,	8,	13,	15,	12,	9,	0,	3,	5,	6,	11}}
	};

	// L'ordine di applicazione delle sbox e' S[0][][] per i 6 bit di msb, fino a s[7][][]
	// per i sei bit di lsb.
	uint8_t indexh, indexl;
	indexh = expandedDataBlock->byte_array[0]>>2;
	indexl = ((expandedDataBlock->byte_array[0]&0x3)<<4) | (expandedDataBlock->byte_array[1]>>4);
	dataBlock->byte_array[0] = (SBOX_indexing(0, indexh)<<4) | SBOX_indexing(1, indexl);
	indexh = ((expandedDataBlock->byte_array[1]&0XF)<<2) | (expandedDataBlock->byte_array[2]>>6);
	indexl = expandedDataBlock->byte_array[2]&0x3F;
	dataBlock->byte_array[1] = (SBOX_indexing(2, indexh)<<4) | SBOX_indexing(3, indexl);
	indexh = expandedDataBlock->byte_array[3]>>2;;
	indexl = ((expandedDataBlock->byte_array[3]&0x3)<<4) | (expandedDataBlock->byte_array[4]>>4);;
	dataBlock->byte_array[2] = (SBOX_indexing(4, indexh)<<4) | SBOX_indexing(5, indexl);
	indexh = ((expandedDataBlock->byte_array[4]&0XF)<<2) | (expandedDataBlock->byte_array[5]>>6);;
	indexl = expandedDataBlock->byte_array[5]&0x3F;;
	dataBlock->byte_array[3] = (SBOX_indexing(6, indexh)<<4) | SBOX_indexing(7, indexl);
}

/**
 * @brief Effettua la permutazione P su un blocco da 32 bit
 * @param[in]	data_in		puntatore a DES_splitted32_t, contenente il blocco da 32 bit da permutare
 * @param[out]	data_out	puntatore a DES_splitted32_t, conterra' il blocco da 32 bit permutato
 */
void DES_PPermutation(DES_splitted32_t *data_in, DES_splitted32_t *data_out) {
	// Permutazione P
	static const uint8_t P[] = {
		16,	25,	12,	11,	3,	20,	4,	15,
		31,	17,	9,	6,	27,	14,	1,	22,
		30,	24,	8,	18,	0,	5,	29,	23,
		13,	19,	2,	26,	10,	21,	28,	7
	};

	int i;
	for (i=0; i<32; i++) {
		if (TestBit(data_in->byte_array, 3, P[i]))
			// anche se permutedKey->byte_array e' di 6 bit anziche' 8, viene considerato essere
			// 8 byte per semplificare la gestione degli indici
			SetBit(data_out->byte_array, 3, 32-i-1);
		else
			ClearBit(data_out->byte_array, 3, 32-i-1);
	}
}

/**
 * @brief Effettua un'operazione di XOR bit a bit tra due blocchi da 32 bit ciascuno
 * @param[in]		L	puntatore a struttura DES_splitted32_t, contenente un blocco da 32 bit
 * @param[inout]	R	puntatore a struttura DES_splitted32_t, contenente l'altro blocco da 32 bit. Successivamente conterra'
 * 						il risultato dell'operazione
 */
void DES_XOR32(DES_splitted32_t *L, DES_splitted32_t *R) {
	int i = 0;
	for (i=0; i<4; i++)
		R->byte_array[i] ^= L->byte_array[i];
}

/**
 * @brief Effettua la permutazione inversa P su un blocco di dati da 64 bit
 * @param[in]	data_in		puntatore a DES_dataBlock_t, contenente il blocco di dati da permutare
 * @param[out]	data_out	puntatore a DES_dataBlock_t, conterra' il blocco di dati permutato
 */
void DES_IIPPermutation(DES_dataBlock_t *data_in, DES_dataBlock_t *data_out) {
	// Permutazione IP inversa
	static const uint8_t invIP[] = {
		24,	56,	16,	48,	8,	40,	0,	32,
		25,	57,	17,	49,	9,	41,	1,	33,
		26,	58,	18,	50,	10,	42,	2,	34,
		27,	59,	19,	51,	11,	43,	3,	35,
		28,	60,	20,	52,	12,	44,	4,	36,
		29,	61,	21,	53,	13,	45,	5,	37,
		30,	62,	22,	54,	14,	46,	6,	38,
		31,	63,	23,	55,	15,	47,	7,	39
	};

	int i;
	for (i=0; i<64; i++) {
		if (TestBit(data_in->byte_array, 7, invIP[i]))
			// anche se permutedKey->byte_array e' di 6 bit anziche' 8, viene considerato essere
			// 8 byte per semplificare la gestione degli indici
			SetBit(data_out->byte_array, 7, 64-i-1);
		else
			ClearBit(data_out->byte_array, 7, 64-i-1);
	}
}

void DES_encrypt(DES_dataBlock_t *plaintext, DES_roundKeys_t *subkeys, DES_dataBlock_t *ciphertext) {
	int i;
	DES_dataBlock_t tmp;
	DES_IPPermutation(plaintext, &tmp);
	DES_splitted32_t L, R;
	DES_split32(&tmp, &L, &R);

	for (i=0; i<16; i++) {
		DES_splitted32_t tmpR;
		COPY_DES_splitted32_t(R, tmpR);
		DES_expanded48_t expandedR;
		DES_expand(&R, &expandedR);
		DES_XOR48(&(subkeys->subkey[i]), &expandedR);
		DES_splitted32_t tmp2;
		DES_SReduce(&expandedR, &tmp2);
		DES_PPermutation(&tmp2, &R);
		DES_XOR32(&L, &R);
		COPY_DES_splitted32_t(tmpR, L);
	}

	DES_merge32(&R, &L, &tmp); //merge e swap, nota R e L invertiti
	DES_IIPPermutation(&tmp, ciphertext);
}

void DES_decrypt(DES_dataBlock_t *ciphertext, DES_roundKeys_t *subkeys, DES_dataBlock_t *plaintext) {
	int i;
	DES_dataBlock_t tmp;
	DES_IPPermutation(ciphertext, &tmp);
	DES_splitted32_t L, R;
	DES_split32(&tmp, &L, &R);

	for (i=0; i<16; i++) {
		DES_splitted32_t tmpR;
		COPY_DES_splitted32_t(R, tmpR);
		DES_expanded48_t expandedR;
		DES_expand(&R, &expandedR);
		DES_XOR48(&(subkeys->subkey[15-i]), &expandedR);
		DES_splitted32_t tmp2;
		DES_SReduce(&expandedR, &tmp2);
		DES_PPermutation(&tmp2, &R);
		DES_XOR32(&L, &R);
		COPY_DES_splitted32_t(tmpR, L);
	}

	DES_merge32(&R, &L, &tmp); //merge e swap, nota R e L invertiti
	DES_IIPPermutation(&tmp, plaintext);
}

