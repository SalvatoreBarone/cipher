/**
 * @file des.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __DES_HEADER_H__
#define __DES_HEADER_H__

/**
 * @addtogroup SimmetricCipher
 * @{
 * @defgroup DES
 * @{
 *
 * @brief Implementazione C dell'algoritmo di cifratura DES.
 */


#include <inttypes.h>


/**
 * @brief Struttura che astrae un blocco dati (plaintext o ciphertext) che l'algoritmo DES deve processare.
 */
typedef struct {
	uint8_t byte_array[8];	/**< array di 8 byte (64 bit) contenente i dati che l'algoritmo DES deve processare */
} DES_dataBlock_t;

/**
 * @brief Struttura che astrae la chiave simmetrica di cifratura/decifratura
 */
typedef struct {
	uint8_t byte_array[8]; /**< array di 8 byte (64 bit) contenente la chiave la chiave simmetrica di cifratura/decifratura */
} DES_key_t;

/**
 * @brief Struttura che astrae una sottochiave a 48 bit, usata dall'algoritmo DES in uno dei round di cifratura/decifratura
 */
typedef struct {
	uint8_t byte_array[6];	/**< Array di 6 byte, 48 bit contenente una sottochiave per uno specifico dei round di cifratura/decifratura */
} DES_roundKey_t;

/**
 * @brief Struttura che astrae le sottochiavi usate dall'algoritmo DES in ciascuno dei round di cifratura/decifratura
 */
typedef struct {
	DES_roundKey_t subkey[16];	/**< Array di 16 DES_roundKey_t, ognuna delle quali contenente una sottochiave per uno
	 	 	 	 	 	 	 	 	 specifico dei round di cifratura/decifratura */
} DES_roundKeys_t;

/**
 * @brief Genera le sottochiavi per ciascuno dei round di codifica
 *
 * @param[in]	key		chiave segreta globale
 * @param[out]	subkeys	puntatore a DES_roundKeys_t, conterra' le sottochiavi che DES usera' in ognuno dei round di cifratura/decifratura
 *
 * @test
 * chiave: 13 34 57 79 9B BC DF F1<br>
 * sottochiavi attese/ottenute:<br>
 * subkey[0]:	1B 02 EF FC 70 72<br>
 * subkey[1]:	79 AE D9 DB C9 E5<br>
 * subkey[2]:	55 FC 8A 42 CF 99<br>
 * subkey[3]:	72 AD D6 DB 35 1D<br>
 * subkey[4]:	7C EC 07 EB 53 A8<br>
 * subkey[5]:	63 A5 3E 50 7B 2F<br>
 * subkey[6]:	EC 84 B7 F6 18 BC<br>
 * subkey[7]:	F7 8A 3A C1 3B FB<br>
 * subkey[8]:	E0 DB EB ED E7 81<br>
 * subkey[9]:	B1 F3 47 BA 46 4F<br>
 * subkey[10]:	21 5F D3 DE D3 86<br>
 * subkey[11]:	75 71 F5 94 67 E9<br>
 * subkey[12]:	97 C5 D1 FA BA 41<br>
 * subkey[13]:	5F 43 B7 F2 E7 3A<br>
 * subkey[14]:	BF 91 8D 3D 3F 0A<br>
 * subkey[15]:	CB 3D 8B 0E 17 F5<br>
 * Si faccia riferimento all'esempio contenuto all'indirizzo<br>
 * http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm<br>
 *
 * @code
 * DES_key_t key = {.byte_array = {0x13, 0x34, 0x57, 0x79, 0x9B, 0xBC, 0xDF, 0xF1}};
 * DES_roundKeys_t subkeys;
 * DES_subkeyGenerator(&key, &subkeys);
 * printf("Sottochiavi:\n");
 * int i, j;
 * for (i=0; i<16; i++) {
 * 	for (j=0; j<6; j++)
 * 		printf("%02X ", subkeys.subkey[i].byte_array[j]);
 * 	printf("\n");
 * }
 * @endcode
 */
void DES_subkeyGenerator(DES_key_t *key, DES_roundKeys_t *subkeys);

/**
 * @brief Cripta un blocco di 64-bit di plaintext usando l'algoritmo DES
 *
 * @param[in]	plaintext	puntatore a struttura DES_dataBlock_t, contenente il blocco di 64-bit di plaintext da criptare
 * @param[in]	subkeys		puntatore a struttura DES_roundKeys_t contenente le sottochiavi che DES usera' in ognuno dei round di
 * 							cifratura/decifratura
 * @param[out]	ciphertext	puntatore a struttura DES_dataBlock_t, conterra' il blocco di 64-bit di ciphertext, prodotto dall'algoritmo
 *
 * @test
 *  plaintext:	02 46 8A CE EC A8 64 20<br>
 *     chiave:	0F 15 71 C9 47 D9 E8 59<br>
 * ciphertext:	DA 02 CE 3A 89 EC AC 3B<br>
 *
 *  plaintext: 01 23 45 67 89 AB CD EF<br>
 *     chiave: 13 34 57 79 9B BC DF F1<br>
 * ciphertext: 85 E8 13 54 0F 0A B4 05<br>
 *
 * @code
 * DES_key_t key = {.byte_array = {0x13, 0x34, 0x57, 0x79, 0x9B, 0xBC, 0xDF, 0xF1}};
 * DES_roundKeys_t subkeys;
 * DES_subkeyGenerator(&key, &subkeys);
 * DES_dataBlock_t plaintext = {.byte_array = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF}};
 * DES_dataBlock_t ciphertext;
 * DES_encrypt(&plaintext, &subkeys, &ciphertext);
 * @endcode
 */
void DES_encrypt(DES_dataBlock_t *plaintext, DES_roundKeys_t *subkeys, DES_dataBlock_t *ciphertext);

/**
 * @brief Decripta un blocco di 64-bit di ciphertext usando l'algoritmo DES
 *
 * @param[in] 	ciphertext	puntatore a struttura DES_dataBlock_t, contenente il blocco di 64-bit di ciphertext da decifrare
 * @param[in] 	subkeys		puntatore a struttura DES_roundKeys_t contenente le sottochiavi che DES usera' in ognuno dei round di
 * 							cifratura/decifratura
 * @param[out] 	plaintext	puntatore a struttura DES_dataBlock_t, contenente il blocco di 64-bit di plaintext decifrato
 *
 * @test
 * ciphertext:	DA 02 CE 3A 89 EC AC 3B<br>
 *     chiave:	0F 15 71 C9 47 D9 E8 59<br>
 *  plaintext:	02 46 8A CE EC A8 64 20<br>
 *
 * ciphertext: 85 E8 13 54 0F 0A B4 05<br>
 *     chiave: 13 34 57 79 9B BC DF F1<br>
 *  plaintext: 01 23 45 67 89 AB CD EF<br>
 *
 *
 * @code
 * DES_key_t key = {.byte_array = {0x13, 0x34, 0x57, 0x79, 0x9B, 0xBC, 0xDF, 0xF1}};
 * DES_roundKeys_t subkeys;
 * DES_subkeyGenerator(&key, &subkeys);
 * DES_dataBlock_t plaintext;
 * DES_decrypt(&ciphertext, &subkeys, &plaintext);
 * @endcode
 */
void DES_decrypt(DES_dataBlock_t *ciphertext, DES_roundKeys_t *subkeys, DES_dataBlock_t *plaintext);

/**
 * @example des_test.c
 * Il file des_test.c contiene un test casuale di cifratura e decifratura, che costituisce anche un esempio
 * d'uso delle funzioni DES_subkeyGenerator(), DES_encrypt() e DES_decrypt().
 * Il test casuale genera chiave di cifratura e plaintext casuale, effettuando una cifratura ed una
 * decifratura, confrontando il plaintext originale con quello ottenuto mediante decifratura del
 * ciphertext.
 */

/**
 * @}
 * @}
 */

#endif
