/**
 * @file aes_test.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "aes.h"
#include <stdio.h>

int main() {

	AES128_key_t key = {
		.byte_array = {0x0F, 0x15, 0x71, 0xC9, 0x47, 0xD9, 0xE8, 0x59, 0x0C, 0xB7, 0xAD, 0xD6, 0xAF, 0x7F, 0x67, 0x98}
	};

	AES_dataBlock_t plaintext = {
		.byte_array = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0xFE, 0xDC, 0xBA, 0x98, 0x76, 0x54, 0x32, 0x10}
	};
	uint8_t expected_ciphertext[16] = {
		0xFF, 0x0B, 0x84, 0x4A, 0x08, 0x53, 0xBF, 0x7C, 0x69, 0x34, 0xAB, 0x43, 0x64, 0x14, 0x8F, 0xB9
	};

	AES_dataBlock_t ciphertext;
	AES_dataBlock_t deciphered;

	AES128_roundKeys_t subkeys;
	// generazione delle sottochiavi di cifratura
	AES128_subkeyGenerator(&key, &subkeys);

	// encryption
	AES128_encrypt(&plaintext, &subkeys, &ciphertext);

	// decription
	AES128_decrypt(&ciphertext, &subkeys, &deciphered);

	return 0;
}
