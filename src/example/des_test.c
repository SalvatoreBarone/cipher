/**
 * @file des_test.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "des.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

	DES_key_t key;
	DES_roundKeys_t subkeys;
	DES_dataBlock_t plaintext;
	DES_dataBlock_t ciphertext;
	DES_dataBlock_t plaintext2;

	srand(time(NULL));

	int i, j, k, numTest = 100000;
	for (j=0; j<numTest; j++) {
		// generazione di chiave e plaintext random
		for (i=0; i<8; i++) {
			key.byte_array[i] = rand();
			plaintext.byte_array[i] = rand();
		}
		// generazione delle chiavi di round
		DES_subkeyGenerator(&key, &subkeys);
		// esecuzione della codifica
		DES_encrypt(&plaintext, &subkeys, &ciphertext);
		// esecuzione della decodifica
		DES_decrypt(&ciphertext, &subkeys, &plaintext2);

		for (i=0; i<8; i++)
			if (plaintext.byte_array[i] != plaintext2.byte_array[i]) {
				printf("Test fallito!\n");
				printf("\tkey: ");
				for (k=0; k<8; k++)
					printf("%02X ", key.byte_array[k]);
				printf("\n\tplaintext: ");
				for (k=0; k<8; k++)
					printf("%02X ", plaintext.byte_array[k]);
				printf("\n");

				exit(-1);
			}
	}

	return 0;
}
