/**
 * @file rc5_test.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "rc5.h"
#include <stdio.h>

int main() {

	// RC5-64/12/16
	uint8_t key[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	uint64_t subkeys[26];
	RC5_subkeyGenerator64(key, 16, 12, subkeys);

	printf("chiavi:\n");
	int i;
	for (i=0; i < 16; i++)
		printf("%016lX\n", subkeys[i]);

	uint64_t plaintext[2] = {0, 0}, ciphertext[2], deciphered[2];
	RC5_encrypt64(plaintext, subkeys, 12, ciphertext);
	printf("\nplaintext: %016lX %016lX\n", plaintext[0], plaintext[1]);
	printf("ciphertext: %016lX %016lX\n", ciphertext[0], ciphertext[1]);
	RC5_decrypt64(ciphertext, subkeys, 12, deciphered);
	printf("deciphered: %016lX %016lX\n", deciphered[0], deciphered[1]);

	return 0;
}
