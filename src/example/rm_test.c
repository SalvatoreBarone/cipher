/**
 * @file rm_test.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "ReedMuller.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// permette di invertire uno dei bit di una codeword
#define bitToggle(row, n) row[15-(n)/8]^=(((uint8_t)1)<<((n)%8))

int main() {
	int i, j;
	RM_1_7_codeword_t codeword;
	uint8_t decoded;
	int errors = 31;	// permette di introdurre una certa dose di errore casuale nella codeword
	srand(time(NULL));
	for (i=0; i<256; i++) {
		//codifica
		RM_1_7_encode((uint8_t)i, &codeword);
		printf("byte=%02x\t\tcodeword=", (uint8_t)i);

		// stampa della codeword
		for (j=0; j<16; j++)
			printf("%02x ", codeword.byte_array[j]);

		// introduzione dell'errore
		for (j=0; j<errors; j++)
			bitToggle(codeword.byte_array, rand()%128);
		// stampa della codeword in cui e' stato introdotto errore casuale
		printf("\terror=");
		for (j=0; j<16; j++)
			printf("%02x ", codeword.byte_array[j]);
		// decodifica
		decoded = RM_1_7_decode(&codeword);
		// stampa del byte decodificato
		printf("\tdecoded=%02x\t", decoded);
		if ((uint8_t)i != decoded) {
			printf("Test fallito!\n");
			return -1;
		}
		printf("\n");
	}
	printf("Test concluso con successo\n");
	return 0;
}
