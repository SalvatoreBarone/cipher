/**
 * @file sha_test.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "sha.h"
#include <stdio.h>

int main() {

//#define __TEST_SHA_256__
#define __TEST_SHA_512__



#ifdef __TEST_SHA_256__

	// test 1
	uint8_t message1[] = {'a', 'b', 'c'};
	SHA256_sum_t hash;
	uint32_t expected1[8] = {0xba7816bf, 0x8f01cfea, 0x414140de, 0x5dae2223, 0xb00361a3, 0x96177a9c, 0xb410ff61, 0xf20015ad};
	SHA256_hashsum(message1, 3, &hash);
	uint8_t i, equals = 1;
	for (i = 0; i < 8; i++)
		if (hash.byte_array[i] != expected1[i])
			equals = 0;
	if (equals)
		printf("Test 1: Il valore hash prodotto da sha256Sum e' corretto\n");
	else
		printf("Test 1: Il valore hash prodotto da sha256Sum NON e' corretto\n");

	// test 2
	uint8_t message2[] = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
	uint32_t expected2[] = {0x248d6a61, 0xd20638b8, 0xe5c02693, 0x0c3e6039, 0xa33ce459, 0x64ff2167, 0xf6ecedd4, 0x19db06c1};
	SHA256_hashsum(message2, 56, &hash);
	equals = 1;
	for (i = 0; i < 8; i++)
		if (hash.byte_array[i] != expected2[i])
			equals = 0;
	if (equals)
		printf("Test 2: Il valore hash prodotto da sha256Sum e' corretto\n");
	else
		printf("Test 2: Il valore hash prodotto da sha256Sum NON e' corretto\n");


	// test 3
	uint8_t message3[] = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
	uint32_t expected3[] = {0x9776cced, 0x80407758, 0x36f4d6af, 0x0605acee, 0xefd39f56, 0x50086a56, 0xd410c0a4, 0x38c05278};
	SHA256_hashsum(message3, 560, &hash);
	equals = 1;
	for (i = 0; i < 8; i++)
		if (hash.byte_array[i] != expected3[i])
			equals = 0;
	if (equals)
		printf("Test 3: Il valore hash prodotto da sha256Sum e' corretto\n");
	else
		printf("Test 3: Il valore hash prodotto da sha256Sum NON e' corretto\n");
#endif



#ifdef __TEST_SHA_512__

	uint8_t message1[] = "abc";
	uint64_t expected1[8] = {
		0xddaf35a193617aba,
		0xcc417349ae204131,
		0x12e6fa4e89a97ea2,
		0x0a9eeee64b55d39a,
		0x2192992a274fc1a8,
		0x36ba3c23a3feebbd,
		0x454d4423643ce80e,
		0x2a9ac94fa54ca49f
	};
	uint8_t message2[] = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopqabcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
	uint64_t expected2[] = {
		0x7361ec4a617b6473,
		0xfb751c44d1026db9,
		0x442915a5fcea1a41,
		0x9e615d2f3bc50694,
		0x94da28b8cf2e4412,
		0xa1dc97d6848f9c84,
		0xa254fb884ad0720a,
		0x83eaa0434aeafd8c
	};
	uint8_t message3[] = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\
abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
	uint64_t expected3[] = {
		0xd2fe01bbdd516f9a,
		0x91d3828e87cad610,
		0x97696e56387bec15,
		0xed8be27fbd2cc3bd,
		0x87fd72e3b8143b02,
		0x48054c7d82872b18,
		0x24c6224727e32432,
		0x32c59e4dfeff3ccf
	};

	SHA512_sum_t hash;
	uint8_t i, equals = 1;
	SHA512_hashsum(message1, 3, &hash);
	for (i = 0; i < 8; i++)
		if (hash.byte_array[i] != expected1[i])
			equals = 0;
	if (equals)
		printf("Test 1: Il valore hash prodotto da sha512Sum e' corretto\n");
	else
		printf("Test 1: Il valore hash prodotto da sha512Sum NON e' corretto\n");


	SHA512_hashsum(message2, 112, &hash);
	equals = 1;
	for (i = 0; i < 8; i++)
		if (hash.byte_array[i] != expected2[i])
			equals = 0;
	if (equals)
		printf("Test 2: Il valore hash prodotto da sha512Sum e' corretto\n");
	else
		printf("Test 2: Il valore hash prodotto da sha512Sum NON e' corretto\n");

	SHA512_hashsum(message3, 560, &hash);
	equals = 1;
	for (i = 0; i < 8; i++)
		if (hash.byte_array[i] != expected3[i])
			equals = 0;
	if (equals)
		printf("Test 3: Il valore hash prodotto da sha512Sum e' corretto\n");
	else
		printf("Test 3: Il valore hash prodotto da sha512Sum NON e' corretto\n");


#endif


	return 0;
}
