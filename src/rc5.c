/**
 * @file rc5.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "rc5.h"

#ifdef __TRACE_RC5__
#include <stdio.h>
#endif

const uint32_t p32 = 0xb7e15163;
const uint32_t q32 = 0x9e3779b9;
const uint64_t p64 = 0xb7e151628ead2a6b;
const uint64_t q64 = 0x9e3779b97f4a7c15;

#define rotRight32(x, y) 	(((x)>>((y)&0x1F)) | ((x)<<(32-((y)&0x1F))))
#define rotLeft32(x, y) 	(((x)<<((y)&0x1F)) | ((x)>>(32-((y)&0x1F))))
#define rotRight64(x, y)	(((x)>>((y)&0x3F)) | ((x)<<(64-((y)&0x3F))))
#define rotLeft64(x, y) 	(((x)<<((y)&0x3F)) | ((x)>>(64-((y)&0x3F))))

void RC5_subkeyGenerator32(uint8_t *key, uint8_t keySize, uint8_t nrounds, uint32_t* subkeys) {
	uint32_t 	L[64];									// prealloco il vettore L. malloc non sempre e' disponibile...
														// se la chiave e' al massimo 256 uint8_t, ho bisogno, al piu', di 256/4 = 64 uint8_t per L
	int		i, k, j,									// i, j e k saranno usati come contatori nel resto della funzione
			c=(keySize>>2)+((keySize&0x3)==0?0:1),			// c e' il numero di uint32_t di L usato
			t = (nrounds+1)<<1,							// t e' il numero di entry del vettore subkeys
			n=3*(t>c?t:c);							// n e' il numero di mix delle sottochiavi

	// copio la chiave nel vettore L
	for (i=keySize-1, L[c-1]=0; i!=-1; i--)
		L[i/4] = (L[i/4]<<8)+key[i];
	// inizializzo la tabella delle chiavi
	for (subkeys[0]=p32, i=1; i<t; i++)
		subkeys[i]=subkeys[i-1]+q32;
	// mixing delle chiavi
	uint32_t A=0, B=0;
	for (k=i=j=0; k<n; k++, i=(i+1)%t, j=(j+1)%c) {
		A = subkeys[i] = rotLeft32(subkeys[i]+A+B, 3);
		B = L[j] = rotLeft32(L[j]+A+B, A+B);
	}
}

void RC5_subkeyGenerator64(uint8_t *key, uint8_t keySize, uint8_t nrounds, uint64_t* subkeys) {
	uint64_t 	L[32];									// prealloco il vettore L. malloc non sempre e' disponibile...
														// se la chiave e' al massimo 256 uint8_t, ho bisogno, al piu', di 256/8 = 32 uint8_t per L
	int	i, k, j,								// i, j e k saranno usati come contatori nel resto della funzione
			c=(keySize>>3)+((keySize&0x7)==0?0:1),		// c e' il numero di uint64_t di L usati
			t = (nrounds+1)<<1,							// t e' il numero di entry del vettore subkeys
			n=3*(t>c?t:c);								// n e' il numero di mix delle sottochiavi

	// copio la chiave nel vettore L
	for (i=keySize-1, L[c-1]=0; i!=-1; i--)
		L[i/8] = (L[i/8]<<8)+key[i];

	// inizializzo la tabella delle chiavi
	for (subkeys[0]=p64, i=1; i<t; i++)
		subkeys[i]=subkeys[i-1]+q64;

	// mixing delle chiavi
	uint64_t A=0, B=0;
	for (k=i=j=0; k<n; k++, i=(i+1)%t, j=(j+1)%c) {
		A = subkeys[i] = rotLeft64(subkeys[i]+A+B, 3);
		B = L[j] = rotLeft64(L[j]+A+B, A+B);
	}
}

void RC5_encrypt32(uint32_t *plaintext, uint32_t* subkeys, uint8_t nrounds, uint32_t *ciphertext) {
	uint8_t i;
	uint32_t 	A=plaintext[0]+subkeys[0],
				B=plaintext[1]+subkeys[1];
	for (i=1; i<=nrounds; i++) {
		A=rotLeft32((A^B), B)+subkeys[i<<1];
		B=rotLeft32((A^B), A)+subkeys[(i<<1)+1];
	}
	ciphertext[0]=A;
	ciphertext[1]=B;
}

void RC5_encrypt64(uint64_t *plaintext, uint64_t* subkeys, uint8_t nrounds, uint64_t *ciphertext) {
	uint8_t i;
	uint64_t 	A=plaintext[0]+subkeys[0],
				B=plaintext[1]+subkeys[1];
	for (i=1; i<=nrounds; i++) {
		A=rotLeft64((A^B), B)+subkeys[i<<1];
		B=rotLeft64((A^B), A)+subkeys[(i<<1)+1];
	}
	ciphertext[0]=A;
	ciphertext[1]=B;
}

void RC5_decrypt32(uint32_t *ciphertext, uint32_t *subkeys, uint8_t nrounds, uint32_t* plaintext) {
	uint8_t i;
	uint32_t	A=ciphertext[0],
				B=ciphertext[1];
	for (i=nrounds; i>0; i--) {
		B=rotRight32((B-subkeys[(i<<1)+1]), A)^A;
		A=rotRight32((A-subkeys[i<<1]), B)^B;
	}
	plaintext[1]=B-subkeys[1];
	plaintext[0]=A-subkeys[0];
}

void RC5_decrypt64(uint64_t *ciphertext, uint64_t *subkeys, uint8_t nrounds, uint64_t* plaintext) {
	uint8_t i;
	uint64_t	A=ciphertext[0],
			B=ciphertext[1];
	for (i=nrounds; i>0; i--) {
		B=rotRight64((B-subkeys[(i<<1)+1]), A)^A;
		A=rotRight64((A-subkeys[i<<1]), B)^B;
	}
	plaintext[1]=B-subkeys[1];
	plaintext[0]=A-subkeys[0];
}
