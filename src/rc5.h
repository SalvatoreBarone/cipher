/**
 * @file rc5.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __RC5_HEADER_H__
#define __RC5_HEADER_H__

#include <inttypes.h>

/**
 * @addtogroup SimmetricCipher
 * @{
 * @defgroup RC5
 * @{
 *
 * @brief Implementazione C dell'algoritmo di cifratura RC5.
 */


/**
 * @brief Generatore di sottochiavi per RC5-32/nrounds/keySize
 * Consente di generare l'opportuno numero di sottochiavi di cifratura per RC5.
 *
 * @param key		array di uint8_t contenente la chiave segreta di cifratura a partire dalla quale saranno generate le sottochiavi
 * @param keySize	dimensione, in byte, dell'array "key"
 * @param nrounds	numero di round di cifratura desiderati; incide sul numero di sottochiavi generate
 * @param subkeys	array che conterra' le sottochiavi; l'array deve essere stato precedentemente allocato e deve avere dimensione opportuna,
 * 					ossia 2*(nrounds+1)
 */
void RC5_subkeyGenerator32(uint8_t *key, uint8_t keySize, uint8_t nrounds, uint32_t* subkeys);

/**
 * @brief Generatore di sottochiavi per RC5-64/nrounds/keySize
 *
 * @param key		array di uint8_t contenente la chiave segreta di cifratura a partire dalla quale saranno generate le sottochiavi
 * @param keySize	dimensione, in byte, dell'array "key"
 * @param nrounds	numero di round di cifratura desiderati; incide sul numero di sottochiavi generate
 * @param subkeys	array che conterra' le sottochiavi; l'array deve essere stato precedentemente allocato e deve avere dimensione opportuna,
 * 					ossia 2*(nrounds+1)
 * @code
 * // RC5-64/12/16
 * uint8_t key[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
 * uint64_t subkeys[26];
 * rc5SubkeyGenerator64(key, 16, 12, subkeys);
 * uint64_t plaintext[2] = {0, 0}, ciphertext[2], deciphered[2];
 * rc5Encrypt64(plaintext, subkeys, 12, ciphertext);
 * printf("\nplaintext: %016lX %016lX\n", plaintext[0], plaintext[1]);
 * printf("ciphertext: %016lX %016lX\n", ciphertext[0], ciphertext[1]);
 * rc5Decrypt64(ciphertext, subkeys, 12, deciphered);
 * printf("deciphered: %016lX %016lX\n", deciphered[0], deciphered[1]);
 * @endcode
 */
void RC5_subkeyGenerator64(uint8_t *key, uint8_t keySize, uint8_t nrounds, uint64_t* subkeys);

/**
 * @brief Effettua la cifratura con l'algoritmo RC5-32/nround/keySize
 *
 * @param plaintext		due uint32_t di input, contengono il plaintext da codificare
 * @param subkeys		array che conterra' le sottochiavi; l'array deve essere stato precedentemente generato usando rc5SubkeyGenerator32
 * @param nrounds		numero di round di cifratura da effettuare, deve coincidere con quello usato per generare le sottochiavi mediante
 * 						rc5SubkeyGenerator32
 * @param ciphertext	due uint32_t di output, conterranno il ciphertext cifrato
 *
 * @code
 * // RC5-32/12/16
 * uint8_t key[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
 * uint32_t subkeys[26];
 * rc5SubkeyGenerator32(key, 16, 12, subkeys);
 * uint32_t plaintext[2] = {0, 0}, ciphertext[2], deciphered[2];
 * rc5Encrypt32(plaintext, subkeys, 12, ciphertext);
 * printf("\nciphertext: %08X %08X\n", ciphertext[0], ciphertext[1]);
 * rc5Decrypt32(ciphertext, subkeys, 12, deciphered);
 * printf("deciphered: %08X %08X\n", deciphered[0], deciphered[1]);
 * @endcode
 *
 * @test
 * plaintext:	00000000 00000000<br>
 * key:			00000000 00000000<br>
 * ciphertext:	EEDBA521 6D8F4B15<br>
 * deciphered:	00000000 00000000<br>
 *
 */
void RC5_encrypt32(uint32_t *plaintext, uint32_t* subkeys, uint8_t nrounds, uint32_t *ciphertext);

/**
 * @brief Effettua la cifratura con l'algoritmo RC5-64/nround/keySize
 *
 * @param plaintext		due uint64_t di input, contengono il plaintext da codificare
 * @param subkeys		array che contiene le sottochiavi; l'array deve essere stato precedentemente generato usando rc5SubkeyGenerator64
 * @param nrounds		numero di round di cifratura da effettuare, deve coincidere con quello usato per generare le sottochiavi mediante
 * 						rc5SubkeyGenerator64
 * @param ciphertext	due uint32_t di output, conterranno il ciphertext cifrato
 *
 * @code
 * // RC5-64/12/16
 * uint8_t key[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
 * uint64_t subkeys[26];
 * rc5SubkeyGenerator64(key, 16, 12, subkeys);
 * uint64_t plaintext[2] = {0, 0}, ciphertext[2], deciphered[2];
 * rc5Encrypt64(plaintext, subkeys, 12, ciphertext);
 * printf("\nplaintext: %016lX %016lX\n", plaintext[0], plaintext[1]);
 * printf("ciphertext: %016lX %016lX\n", ciphertext[0], ciphertext[1]);
 * rc5Decrypt64(ciphertext, subkeys, 12, deciphered);
 * printf("deciphered: %016lX %016lX\n", deciphered[0], deciphered[1]);
 * @endcode
 *
 * @test
 * plaintext:   0000000000000000 0000000000000000<br>
 * key:		    0000000000000000 0000000000000000<br>
 * ciphertext:  9ADC4F9D5CA5A026 56618780997C6C6F<br>
 * deciphered:  0000000000000000 0000000000000000<br>
 */
void RC5_encrypt64(uint64_t *plaintext, uint64_t* subkeys, uint8_t nrounds, uint64_t *ciphertext);

/**
 * @brief Effettua la decifratura di uint32_t precedentemente cifrate con RC5-32/nround/keySize
 *
 * @param ciphertext	due uint32_t di input, contengono il ciphertext da decodificare
 * @param subkeys		array che conterra' le sottochiavi; l'array deve essere stato precedentemente generato usando rc5SubkeyGenerator32
 * @param nrounds		numero di round di cifratura da effettuare, deve coincidere con quello usato per generare le sottochiavi mediante
 * 						rc5SubkeyGenerator32
 * @param plaintext		due uint32_t di output, conterranno il plaintext decodificato
 *
 * @code
 * // RC5-32/12/16
 * uint8_t key[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
 * uint32_t subkeys[26];
 * rc5SubkeyGenerator32(key, 16, 12, subkeys);
 * uint32_t plaintext[2] = {0, 0}, ciphertext[2], deciphered[2];
 * rc5Encrypt32(plaintext, subkeys, 12, ciphertext);
 * printf("\nciphertext: %08X %08X\n", ciphertext[0], ciphertext[1]);
 * rc5Decrypt32(ciphertext, subkeys, 12, deciphered);
 * printf("deciphered: %08X %08X\n", deciphered[0], deciphered[1]);
 * @endcode
 *
 * @test
 * plaintext:	00000000 00000000<br>
 * key:			00000000 00000000<br>
 * ciphertext:	EEDBA521 6D8F4B15<br>
 * deciphered:	00000000 00000000<br>
 */
void RC5_decrypt32(uint32_t *ciphertext, uint32_t *subkeys, uint8_t nrounds, uint32_t* plaintext);

/**
 * @brief Effettua la decifratura di uint32_t precedentemente cifrate con RC5-64/nround/keySize
 *
 * @param ciphertext	due uint64_t di input, contengono il ciphertext da decodificare
 * @param subkeys		array che contiene le sottochiavi; l'array deve essere stato precedentemente generato usando rc5SubkeyGenerator64
 * @param nrounds		numero di round di cifratura da effettuare, deve coincidere con quello usato per generare le sottochiavi mediante
 * 						rc5SubkeyGenerator64
 * @param plaintext		due uint64_t di output, conterranno il plaintext decodificato
 *
 * @code
 * // RC5-64/12/16
 * uint8_t key[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
 * uint64_t subkeys[26];
 * rc5SubkeyGenerator64(key, 16, 12, subkeys);
 * uint64_t plaintext[2] = {0, 0}, ciphertext[2], deciphered[2];
 * rc5Encrypt64(plaintext, subkeys, 12, ciphertext);
 * printf("\nplaintext: %016lX %016lX\n", plaintext[0], plaintext[1]);
 * printf("ciphertext: %016lX %016lX\n", ciphertext[0], ciphertext[1]);
 * rc5Decrypt64(ciphertext, subkeys, 12, deciphered);
 * printf("deciphered: %016lX %016lX\n", deciphered[0], deciphered[1]);
 * @endcode
 *
 * @test
 * plaintext:  0000000000000000 0000000000000000<br>
 * key:		   0000000000000000 0000000000000000<br>
 * ciphertext: 9ADC4F9D5CA5A026 56618780997C6C6F<br>
 * deciphered: 0000000000000000 0000000000000000<br>
 */
void RC5_decrypt64(uint64_t *ciphertext, uint64_t *subkeys, uint8_t nrounds, uint64_t* plaintext);

/**
 * @example rc5_test.c
 * Il file rc5_test contiene un esempio d'uso delle funzioni RC5_subkeyGenerator64(), RC5_encrypt64() ed
 * RC5_decrypt64(), usate con una chiave di cifratura lunga 16 byte e 12 round di cifratura.
 */

/**
 * @}
 * @}
 */

#endif
