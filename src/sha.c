/**
 * @file sha.c
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include "sha.h"

#ifdef __TRACE_SHA__
#include <stdio.h>
#endif

// shift circolare di x a destra di y-bit. Per uint32_t.
#define circularRightShift32(x, y)	(((x)>>((y)&0x1F)) | ((x)<<(32-((y)&0x1F))))

// shift circolare di x a sinistra di y-bit. Per uint32_t.
#define circularLeftShift32(x, y) 	(((x)<<((y)&0x1F)) | ((x)>>(32-((y)&0x1F))))

// shift circolare di x a destra di y-bit. Per uint64_t.
#define circularRightShift64(x, y)	(((x)>>((y)&0x3F)) | ((x)<<(64-((y)&0x3F))))

// shift circolare di x a sinistra di y-bit. Per uint64_t.
#define circularLeftShift64(x, y) 	(((x)<<((y)&0x3F)) | ((x)>>(64-((y)&0x3F))))

// rotazione "A" usata nel uint32_t-scheduling in SHA512
#define sha512RotateWsA(x) 				(circularRightShift64(x, 1) ^ circularRightShift64(x, 8) ^ (x>>7))

// rotazione "B" usata nel uint32_t-scheduling in SHA512
#define sha512RotateWsB(x) 				(circularRightShift64(x, 19) ^ circularRightShift64(x, 61) ^ (x>>6))

// rotazione "A" usata nel uint32_t-scheduling in SHA256
#define sha256RotateWsA(x) 				(circularRightShift32(x, 7) ^ circularRightShift32(x, 18) ^ (x>>3))

// rotazione "B" usata nel uint32_t-scheduling in SHA256
#define sha256RotateWsB(x) 				(circularRightShift32(x, 17) ^ circularRightShift32(x, 19) ^ (x>>10))

// rotazione "A" usata in un round SHA512
#define sha512RotateRA(x)				(circularRightShift64(x, 28) ^ circularRightShift64(x, 34) ^ circularRightShift64(x, 39))

// rotazione "B" usata in un round SHA512
#define sha512RotateRB(x)				(circularRightShift64(x, 14) ^ circularRightShift64(x, 18) ^ circularRightShift64(x, 41))

// rotazione "A" usata in un round SHA256
#define sha256RotateRA(x)				(circularRightShift32(x, 2) ^ circularRightShift32(x, 13) ^ circularRightShift32(x, 22))

// rotazione "B" usata in un round SHA256
#define sha256RotateRB(x)				(circularRightShift32(x, 6) ^ circularRightShift32(x, 11) ^ circularRightShift32(x, 25))

// funzione "Ch" usata in un round SHA512 o SHA256
#define Ch(a, b, c)					((a & b)^(~a & c))

// funzione "Maj" usata in un round SHA512 o SHA256
#define Maj(a, b, c)				((a & b) ^ (a & c) ^ (b & c))


const uint64_t sha256K[80] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};


const uint64_t sha512K[80] = {
	0x428a2f98d728ae22,	0x7137449123ef65cd,	0xb5c0fbcfec4d3b2f,	0xe9b5dba58189dbbc,	0x3956c25bf348b538,	0x59f111f1b605d019,	0x923f82a4af194f9b,	0xab1c5ed5da6d8118,
	0xd807aa98a3030242,	0x12835b0145706fbe,	0x243185be4ee4b28c,	0x550c7dc3d5ffb4e2,	0x72be5d74f27b896f,	0x80deb1fe3b1696b1,	0x9bdc06a725c71235,	0xc19bf174cf692694,
	0xe49b69c19ef14ad2,	0xefbe4786384f25e3,	0x0fc19dc68b8cd5b5,	0x240ca1cc77ac9c65,	0x2de92c6f592b0275,	0x4a7484aa6ea6e483,	0x5cb0a9dcbd41fbd4,	0x76f988da831153b5,
	0x983e5152ee66dfab,	0xa831c66d2db43210,	0xb00327c898fb213f,	0xbf597fc7beef0ee4,	0xc6e00bf33da88fc2,	0xd5a79147930aa725,	0x06ca6351e003826f,	0x142929670a0e6e70,
	0x27b70a8546d22ffc,	0x2e1b21385c26c926,	0x4d2c6dfc5ac42aed,	0x53380d139d95b3df,	0x650a73548baf63de,	0x766a0abb3c77b2a8,	0x81c2c92e47edaee6,	0x92722c851482353b,
	0xa2bfe8a14cf10364,	0xa81a664bbc423001,	0xc24b8b70d0f89791,	0xc76c51a30654be30,	0xd192e819d6ef5218,	0xd69906245565a910,	0xf40e35855771202a,	0x106aa07032bbd1b8,
	0x19a4c116b8d2d0c8,	0x1e376c085141ab53,	0x2748774cdf8eeb99,	0x34b0bcb5e19b48a8,	0x391c0cb3c5c95a63,	0x4ed8aa4ae3418acb,	0x5b9cca4f7763e373,	0x682e6ff3d6b2b8a3,
	0x748f82ee5defb2fc,	0x78a5636f43172f60,	0x84c87814a1f0ab72,	0x8cc702081a6439ec,	0x90befffa23631e28,	0xa4506cebde82bde9,	0xbef9a3f7b2c67915,	0xc67178f2e372532b,
	0xca273eceea26619c,	0xd186b8c721c0c207,	0xeada7dd6cde0eb1e,	0xf57d4f7fee6ed178,	0x06f067aa72176fba,	0x0a637dc5a2c898a6,	0x113f9804bef90dae,	0x1b710b35131c471b,
	0x28db77f523047d84,	0x32caab7b40c72493,	0x3c9ebe0a15c9bebc,	0x431d67c49c100d4c,	0x4cc5d4becb3e42b6,	0x597f299cfc657e2a,	0x5fcb6fab3ad6faec,	0x6c44198c4a475817
};

void sha512WordScheduling(uint64_t inputBlock[16], uint64_t uint32_ts[80]) {
	int i;
	for (i = 0; i < 16; i++)
		uint32_ts[i] = inputBlock[i];
	for (i=16; i<80; i++)
		uint32_ts[i] = sha512RotateWsB(uint32_ts[i-2]) + uint32_ts[i-7] + sha512RotateWsA(uint32_ts[i-15]) + uint32_ts[i-16];
}

void sha256WordScheduling(uint32_t inputBlock[16], uint32_t uint32_ts[64]) {
	int i;
	for (i = 0; i < 16; i++)
		uint32_ts[i] = inputBlock[i];
	for (i=16; i<64; i++)
		uint32_ts[i] = sha256RotateWsB(uint32_ts[i-2]) + uint32_ts[i-7] + sha256RotateWsA(uint32_ts[i-15]) + uint32_ts[i-16];
}

void sha512Round(uint64_t buffer[8], uint64_t Wt, uint64_t Kt) {
	uint64_t T1 = buffer[7] + Ch(buffer[4], buffer[5], buffer[6]) + sha512RotateRB(buffer[4]) + Wt + Kt;
	uint64_t T2 = sha512RotateRA(buffer[0]) + Maj(buffer[0], buffer[1], buffer[2]);
	buffer[7] = buffer[6];
	buffer[6] = buffer[5];
	buffer[5] = buffer[4];
	buffer[4] = buffer[3] + T1;
	buffer[3] = buffer[2];
	buffer[2] = buffer[1];
	buffer[1] = buffer[0];
	buffer[0] = T1 + T2;
}

void sha256Round(uint32_t buffer[8], uint32_t Wt, uint32_t Kt) {
	uint32_t T1 = buffer[7] + Ch(buffer[4], buffer[5], buffer[6]) + sha256RotateRB(buffer[4]) + Wt + Kt;
	uint32_t T2 = sha256RotateRA(buffer[0]) + Maj(buffer[0], buffer[1], buffer[2]);
	buffer[7] = buffer[6];
	buffer[6] = buffer[5];
	buffer[5] = buffer[4];
	buffer[4] = buffer[3] + T1;
	buffer[3] = buffer[2];
	buffer[2] = buffer[1];
	buffer[1] = buffer[0];
	buffer[0] = T1 + T2;
}

void sha512Fbox(uint64_t inputBlock[16], uint64_t chainingVariable[8], uint64_t hashValue[8]) {
#ifdef __TRACE_SHA__
	printf("input uint64_t:\n");
	printf("\t%016lX %016lX %016lX %016lX\n", inputBlock[0], inputBlock[1], inputBlock[2], inputBlock[3]);
	printf("\t%016lX %016lX %016lX %016lX\n", inputBlock[4], inputBlock[5], inputBlock[6], inputBlock[7]);
	printf("\t%016lX %016lX %016lX %016lX\n", inputBlock[8], inputBlock[9], inputBlock[10], inputBlock[11]);
	printf("\t%016lX %016lX %016lX %016lX\n", inputBlock[12], inputBlock[13], inputBlock[14], inputBlock[15]);
#endif
	uint64_t W[80];
	sha512WordScheduling(inputBlock, W);
	int i;
	for (i = 0; i < 8; i++)
		hashValue[i] = chainingVariable[i];
#ifdef __TRACE_SHA__
		printf("init:\n");
		printf("\t%016lX %016lX %016lX %016lX\n", hashValue[0], hashValue[1], hashValue[2], hashValue[3]);
		printf("\t%016lX %016lX %016lX %016lX\n", hashValue[4], hashValue[5], hashValue[6], hashValue[7]);
#endif
	for (i = 0; i < 80; i++) {
		sha512Round(hashValue, W[i], sha512K[i]);
#ifdef __TRACE_SHA__
		printf("round: %d\n", i);
		printf("\t%016lX %016lX %016lX %016lX\n", hashValue[0], hashValue[1], hashValue[2], hashValue[3]);
		printf("\t%016lX %016lX %016lX %016lX\n", hashValue[4], hashValue[5], hashValue[6], hashValue[7]);
#endif
	}
	for (i = 0; i < 8; i++)
		hashValue[i] += chainingVariable[i];
#ifdef __TRACE_SHA__
		printf("final hash value:\n");
		printf("\t%016lX %016lX %016lX %016lX\n", hashValue[0], hashValue[1], hashValue[2], hashValue[3]);
		printf("\t%016lX %016lX %016lX %016lX\n", hashValue[4], hashValue[5], hashValue[6], hashValue[7]);
#endif
}

void sha256Fbox(uint32_t inputBlock[16], uint32_t chainingVariable[8], uint32_t hashValue[8]) {
#ifdef __TRACE_SHA__
	printf("input uint64_t:\n");
	printf("\t%08X %08X %08X %08X\n", inputBlock[0], inputBlock[1], inputBlock[2], inputBlock[3]);
	printf("\t%08X %08X %08X %08X\n", inputBlock[4], inputBlock[5], inputBlock[6], inputBlock[7]);
	printf("\t%08X %08X %08X %08X\n", inputBlock[8], inputBlock[9], inputBlock[10], inputBlock[11]);
	printf("\t%08X %08X %08X %08X\n", inputBlock[12], inputBlock[13], inputBlock[14], inputBlock[15]);

#endif
	uint32_t W[64];
	sha256WordScheduling(inputBlock, W);
	int i;
	for (i = 0; i < 8; i++)
		hashValue[i] = chainingVariable[i];
#ifdef __TRACE_SHA__
		printf("init:\n");
		printf("\t%08X %08X %08X %08X\n", hashValue[0], hashValue[1], hashValue[2], hashValue[3]);
		printf("\t%08X %08X %08X %08X\n", hashValue[4], hashValue[5], hashValue[6], hashValue[7]);
#endif
	for (i = 0; i < 64; i++) {
		sha256Round(hashValue, W[i], sha256K[i]);
#ifdef __TRACE_SHA__
		printf("round: %d\n", i);
		printf("\t%08X %08X %08X %08X\n", hashValue[0], hashValue[1], hashValue[2], hashValue[3]);
		printf("\t%08X %08X %08X %08X\n", hashValue[4], hashValue[5], hashValue[6], hashValue[7]);
#endif
	}
	for (i = 0; i < 8; i++)
		hashValue[i] += chainingVariable[i];
#ifdef __TRACE_SHA__
	printf("final hash value:\n");
	printf("\t%08X %08X %08X %08X\n", hashValue[0], hashValue[1], hashValue[2], hashValue[3]);
	printf("\t%08X %08X %08X %08X\n", hashValue[4], hashValue[5], hashValue[6], hashValue[7]);
#endif
}

void SHA512_hashsum(uint8_t* inputData, uint64_t size, SHA512_sum_t *hashValue) {
	uint64_t chainingVariable[8] = {
		0x6A09E667F3BCC908,
		0xBB67AE8584CAA73B,
		0x3C6EF372FE94F82B,
		0xA54FF53A5F1D36F1,
		0x510E527FADE682D1,
		0x9B05688C2B3E6C1F,
		0x1F83D9ABFB41BD6B,
		0x5BE0CD19137E2179
	};

	uint64_t uint8_tOfPad, totalLength;
	if (size % 128 == 112)
		uint8_tOfPad = 128;
	else
		uint8_tOfPad = 112 - (size % 128);
	totalLength = size + uint8_tOfPad + 16;
#ifdef __TRACE_SHA__
	printf("size: %lu\npad: %lu\ntotal: %lu\n", size, uint8_tOfPad, totalLength);
#endif

	uint64_t data[16];										// vettore tampone, riempito progressivamente con i dati di input/padding,
															// usato per il calcolo dell'hash
	uint8_t *ptr = (uint8_t*)data;							// puntatore usato per scorrere nel vettore tampone un uint8_t per volta
	uint64_t i;
	int j;
	for (i = 7; i < totalLength; i+=8) {					// scorre il vettore dei dati di input di otto uint8_t in otto uint8_t
		for (j = 0; j< 8; j++, ptr++)						// copia i uint8_t dal vettore di input nel vettore tampone oppure effettua il padding
															// on-the-fly
			if ((i-j) < size)
				*ptr = inputData[i-j];
			else if ((i-j) == size)
				*ptr = 0x80;
			else
				*ptr = 0x00;
		if ((i+1)%128==0) {									// completato il riempimento del vettore tampone, esso pue' essere usato per il calcolo
															// dell'hash
#ifdef __TRACE_SHA__
	printf("i: %lu\n", i);
#endif
			if (i == totalLength - 1) {						// se si sta per processare l'ultima partizione, nelle ultime due uint32_t viene inserita
															// la dimensione
															// del messaggio originale, senza padding, in bit
					data[14] = size>>61;
					data[15] = size<<3;
			}
			else
				ptr = (uint8_t*)data;						// il puntatore viene riportato all'inizio del vettore tampone
			sha512Fbox(data, chainingVariable, hashValue->byte_array);	// calcolo dell'hash parziale
			for (j = 0; j < 8; j++)							// copia dell'hash parziale nella chaining-variable
				chainingVariable[j] = hashValue->byte_array[j];
		}
	}
#ifdef __TRACE_SHA__
	printf("i: %lu\n", i);
#endif
}

void SHA256_hashsum(uint8_t* inputData, uint64_t size, SHA256_sum_t *hashValue) {
	uint32_t chainingVariable[8] = {
		0x6A09E667,
		0xBB67AE85,
		0x3C6EF372,
		0xA54FF53A,
		0x510E527F,
		0x9B05688C,
		0x1F83D9AB,
		0x5BE0CD19
	};

	uint64_t uint8_tOfPad, totalLength;
	if (size % 64 == 56)
		uint8_tOfPad = 64;
	else
		uint8_tOfPad = 56 - (size % 64);
	totalLength = size + uint8_tOfPad + 8;
#ifdef __TRACE_SHA__
	printf("size: %lu\npad: %lu\ntotal: %lu\n", size, uint8_tOfPad, totalLength);
#endif

	uint32_t data[16];										// vettore tampone, riempito progressivamente con i dati di input/padding,
															// usato per il calcolo dell'hash
	uint8_t *ptr = (uint8_t*)data;							// puntatore usato per scorrere nel vettore tampone un uint8_t per volta
	uint64_t i;
	int j;
	for (i = 3; i < totalLength; i+=4) {					// scorre il vettore dei dati di input di quattro uint8_t in quattro uint8_t
		for (j = 0; j< 4; j++, ptr++)						// copia i uint8_t dal vettore di input nel vettore tampone oppure effettua il padding
															// on-the-fly
			if ((i-j) < size)
				*ptr = inputData[i-j];
			else if ((i-j) == size)
				*ptr = 0x80;
			else
				*ptr = 0x00;
		if ((i+1)%64==0) {									// completato il riempimento del vettore tampone, esso pue' essere usato per il calcolo
															// dell'hash
#ifdef __TRACE_SHA__
	printf("i: %lu\n", i);
#endif
			if (i == totalLength-1) {						// se si sta per processare l'ultima partizione, nelle ultime due uint32_t viene inserita
															// la dimensione
															// del messaggio originale, senza padding, in bit
					data[14] = (uint32_t)(size>>29);
					data[15] = (uint32_t)(size<<3);
			}
			else
				ptr = (uint8_t*)data;						// il puntatore viene riportato all'inizio del vettore tampone
			sha256Fbox(data, chainingVariable, hashValue->byte_array);	// calcolo dell'hash parziale
			for (j = 0; j < 8; j++)							// copia dell'hash parziale nella chaining-variable
				chainingVariable[j] = hashValue->byte_array[j];
		}
	}
#ifdef __TRACE_SHA__
	printf("i: %lu\n", i);
#endif
}
