/**
 * @file sha.h
 *
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Cipher
 *
 * Cipher is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Cipher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SHA_HEADER_H__
#define __SHA_HEADER_H__

/**
 * @addtogroup Hashing
 * @{
 *
 * @brief Implementazione C di alcune funzioni di Hashing.
 *
 * @defgroup SHA
 * @{
 *
 * @brief Implementazione C delle funzioni hash SHA-256 e SHA-512
 */

#include <inttypes.h>

/**
 * @brief Struttura che astrae l'hashsum prodotto da SHA-512
 */
typedef struct {
	uint64_t byte_array[8]; /**< array di 64 byte, organizzati in otto word da otto byte, contenenti l'hashsum prodotto da SHA-512 */
} SHA512_sum_t;

/**
 * @brief Struttura che astrae l'hashsum prodotto da SHA-256
 */
typedef struct {
	uint32_t byte_array[8]; /**< array di 32 byte, organizzati in otto word da quattro byte, contenenti l'hashsum prodotto da SHA-256 */
} SHA256_sum_t;


/**
 * @brief Calcola l'hashsum SHA-512
 * @param[in]	inputData		vettore di byte di cui si desidera calcolare l'hashsum
 * @param[in]	size			dimensione, in byte, del vettore inputData
 * @param[out]	hashValue		puntatore a struttura SHA512_sum_t, essa conterra' l'hashsum calcolato dall'algoritmo
 *
 * @code
 * 	SHA512_sum_t hash;
 * 	uint8_t i, equals = 1;
 * 	SHA512_hashsum(message1, 3, &hash);
 * 	for (i = 0; i < 8; i++)
 * 		if (hash.byte_array[i] != expected1[i])
 * 			equals = 0;
 * 	if (equals)
 * 		printf("Test 1: Il valore hash prodotto da sha512Sum e' corretto\n");
 * 	else
 * 		printf("Test 1: Il valore hash prodotto da sha512Sum NON e' corretto\n");
 * @endcode
 *
 * @test
 * Input: "abc";<br>
 * Output: DDAF35A193617ABA CC417349AE204131 12E6FA4E89A97EA2 0A9EEEE64B55D39A<br>
 * 		   2192992A274FC1A8 36BA3C23A3FEEBBD 454D4423643CE80E 2A9AC94FA54CA49F<br>
 * Input: "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopqabcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"<br>
 * Output: 7361EC4A617B6473 FB751C44D1026DB9 442915A5FCEA1A41 9E615D2F3BC50694<br>
 * 		   94DA28B8CF2E4412 A1DC97D6848F9C84 A254FB884AD0720A 83EAA0434AEAFD8C<br>
 * Input:<br>
 * 		"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" <br>
 * Output:<br>
 * 		D2FE01BBDD516F9A 91D3828E87CAD610 97696E56387BEC15 ED8BE27FBD2CC3BD<br>
 * 		87FD72E3B8143B02 48054C7D82872B18 24C6224727E32432 32C59E4DFEFF3CCF<br>
 */
void SHA512_hashsum(uint8_t* inputData, uint64_t size, SHA512_sum_t *hashValue);

/**
 * @brief Calcola l'hashsum SHA-256
 *
 * @param[in]	inputData		vettore di byte di cui si desidera calcolare l'hashsum
 * @param[in]	size			dimensione, in byte, del vettore inputData
 * @param[out]	hashValue		puntatore a struttura SHA256_sum_t, essa conterra' l'hashsum calcolato dall'algoritmo
 *
 * @code
 * 	// test 1
 * uint8_t message1[] = {'a', 'b', 'c'};
 * SHA256_sum_t hash;
 * uint32_t expected1[8] = {0xba7816bf, 0x8f01cfea, 0x414140de, 0x5dae2223, 0xb00361a3, 0x96177a9c, 0xb410ff61, 0xf20015ad};
 * SHA256_hashsum(message1, 3, &hash);
 * uint8_t i, equals = 1;
 * for (i = 0; i < 8; i++)
 * 	if (hash.byte_array[i] != expected1[i])
 * 		equals = 0;
 * if (equals)
 * 	printf("Test 1: Il valore hash prodotto da sha256Sum e' corretto\n");
 * else
 * 	printf("Test 1: Il valore hash prodotto da sha256Sum NON e' corretto\n");
 * @endcode
 *
 * @test Test eseguito il 25 marzo 2017.<br>
 * La funzione ha mostrato il comportamento corretto<br>
 * Input:<br>
 * 		"abc";<br>
 * Output:<br>
 * 		BA7816BF 8F01CFEA 414140DE 5DAE2223<br>
 * 		B00361A3 96177A9C B410FF61 F20015AD<br>
 *<br>
 * Input:<br>
 * 		"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"<br>
 * Output:<br>
 * 		248D6A61 D20638B8 E5C02693 0C3E6039<br>
 * 		A33CE459 64FF2167 F6ECEDD4 19DB06C1<br>
 *<br>
 * Input:<br>
 * 		"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\ <br>
 * 		abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" <br>
 * Output:<br>
 * 		9776CCED 80407758 36F4D6AF 0605ACEE<br>
 * 		EFD39F56 50086A56 D410C0A4 38C05278<br>
 */
void SHA256_hashsum(uint8_t* inputData, uint64_t size, SHA256_sum_t *hashValue);

/**
 * @example sha_test.c
 * Il file sha_test.c costituisce un esempio d'uso delle funzioni SHA512_hashsum() e SHAs56_hashsum().
 * Nell'esempio vengono calcolati i valori hash di diverse stringhe di byte, confrontando il valore ottenuto
 * con un valore atteso.
 */

/**
 * @}
 * @}
 */

#endif
